<!-- edit_branch.php (View) -->

<?=$this->extend('admin/admin')?>
<?=$this->section('content');?>

<div class="container mt-4">
    <h2>Edit Branch</h2>
    <form method="post" action="<?= base_url('branch/update/'.$branch['id']) ?>" name="add_create" id="add_create">
        <div class="mb-3">
            <label for="branch_name" class="form-label">Branch Name:</label>
            <input type="text" name="branch_name" id="branch_name" class="form-control" value="<?= $branch['branch_name'] ?>" required>
        </div>

        <div class="mb-3">
            <label for="branch_location" class="form-label">Branch Location:</label>
            <input type="text" name="branch_location" id="branch_location" class="form-control" value="<?= $branch['branch_location'] ?>" required>
        </div>

        <div class="mb-3">
            <label for="status" class="form-label">Status:</label>
            <select name="status" id="status" class="form-select form-control" required>
                <option value="active" <?= ($branch['status'] === 'active') ? 'selected' : '' ?>>Active</option>
                <option value="inactive" <?= ($branch['status'] === 'inactive') ? 'selected' : '' ?>>Inactive</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Update Branch</button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_create").length > 0) {
  $("#add_create").validate({
    rules: {
        branch_name: {
        required: true,
      },
      branch_location: {
        required: true,
      },
      status: {
        required: true,
      },
    }, 

    messages: {
        branch_name: {
        required: "branch  Name is required.",
      },
      branch_location: {
        required: "Last Name is required.",
      },
      status: {
        required: "Please select Your status.",
      },
    },
  });
}

  </script>
<?=$this->endsection();?>
