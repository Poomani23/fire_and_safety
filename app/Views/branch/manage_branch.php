<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

     <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
                   
                    <!-- DataTales Example -->
                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Manage Branch</h6>
                            <div class="text-right">
                           
                          
                        <a href="<?php echo site_url('add_branch') ?>" class="btn btn-primary btn-icon-split">   
                <span class="icon text-white-900">
                    <i class="fa fa-plus"></i>
                </span>
                <span class="text">ADD</span>
            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <!-- Table Header -->
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Location</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <!-- Loop through branches -->
             <?php 
                $id = 1;
                foreach ($branches as $branch): 
             ?>
                <tr>
                    <td><?= $id;?></td>
                    <td><?= $branch['branch_name'] ?></td>
                    <td><?= $branch['branch_location'] ?></td>
                    <td><?= $branch['status'] ?></td>
                   
                    <td>
                <a href="<?= base_url('branch/edit/'.$branch['id']) ?>"><i class="fas fa-edit" style="color:green"></i></a>|
            <a href="<?= base_url('branch/delete/'.$branch['id']) ?>"><i class="fa fa-trash"style="color:red" ></i> </a>

                                            </td>
                </tr>
                <?php $id++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div></div>


    <!-- Page level plugins -->
   
<?=$this->endsection();?>
