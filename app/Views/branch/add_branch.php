<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="text-right">
        <a href="<?= base_url('manage_branch') ?>" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-900">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Branch</h1>
            </div>
            <form method="post" action="<?= base_url('branch/save'); ?>" class="user" name="add_create" id="add_create">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="branch_name" placeholder="Branch Name" name="branch_name">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="branch_location" placeholder="Branch Location" name="branch_location">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <select name="status" id="status" class="form-select form-control" required>
                            <option value="">Select Status</option>
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                    </div>
                </div>
                <input type="submit" name="submit" value="Add Branch" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_create").length > 0) {
  $("#add_create").validate({
    rules: {
        branch_name: {
        required: true,
      },
      branch_location: {
        required: true,
      },
      status: {
        required: true,
      },
    }, 

    messages: {
        branch_name: {
        required: "branch  Name is required.",
      },
      branch_location: {
        required: "Last Name is required.",
      },
      status: {
        required: "Please select Your status.",
      },
    },
  });
}

  </script>
<?=$this->endsection();?>
