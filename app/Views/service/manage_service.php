<!-- manage_service.php -->

<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Services</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Services</h6>
            <div class="text-right">
                <a href="<?php echo site_url('add_service') ?>" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-900">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">ADD</span>
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <!-- Table Header -->
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Service Item</th>
                            <th>Description</th>
                            <th>Price and Branch Location</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Loop through services -->
                        <?php
                        $id = 1;
                        foreach ($services as $service):
                        ?>
                            <tr>
                                <td><?= $id;?></td>
                                <td><?= $service['sr_item'] ?></td>
                                <td><?= $service['description'] ?></td>
                                <td>
                                    <!-- Fetch prices for the current service -->
                                    <?php $servicePrices = $servicePricesModel->getPricesByServiceId($service['id']); ?>

                                    <!-- Loop through prices for branches -->
                                    <?php foreach ($servicePrices as $price): ?>
                                        <?php
                                        // Get branch location for the current branch_id
                                        $branchId = $price['branch_id'];
                                        $branchLocation = $servicePricesModel->getLocationByBranchId($branchId);
                                         
                                        ?>
                                        <p>
                                            Branch: <?= $branchLocation ?? 'Branch Location Not Available' ?> | Price: <?= $price['price'] ?> 
                                        </p>
                                    <?php endforeach; ?>
                                </td>
                                <td><?= $service['status'] ?></td>
                                <td>
                                    <a href="<?= site_url('edit_service/' . $service['id']) ?>"><i class="fas fa-edit"></i></a> |
                                    <a href="<?= site_url('delete_service/' . $service['id']) ?>" ><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php $id++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<?=$this->endsection();?>
