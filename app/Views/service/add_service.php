<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="text-right">
        <a href="<?= base_url('manage_service') ?>" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-900">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Service</h1>
            </div>
            <form method="post" action="<?= base_url('store'); ?>" class="user" name="add_product" id="add_product" enctype="multipart/form-data">
            <div class="form-group row">
            <div class="col-sm-6">
    <select class="form-control" id="category" name="category">
        <option value="">Select Category</option>
        <?php foreach ($categories as $category): ?>
            <option value="<?= $category['category_name']; ?>"><?= $category['category_name']; ?></option>
        <?php endforeach; ?>
    </select>
</div>



                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="sr_item" placeholder="Service Item" name="sr_item">
                    </div>
                </div>
               
                <div class="form-group row">
                
                   
                     <div class="col-sm-6">
             <textarea class="form-control" id="description" placeholder=" Description" name="description"></textarea> 
                    </div>
                     <div class="col-sm-6 mb-3 mb-sm-0">
                        <select name="status" id="status" class="form-select form-control" required>
                            <option value="">Select Status</option>
                            <option value="available">Available</option>
                            <option value="Unavailable">Unavailable</option>
                        </select>
                    </div>
                </div>
                <div class ="form-group row">
               
                    </div>
                   
                   
                    <div class="form-group">
         <?php foreach ($branches as $branch): ?>
        <div class="row mb-3">
            <div class="col-sm-2">
                <label for="branch_<?= $branch['id'] ?>" class="mr-2"><?= $branch['branch_location'] ?>:</label>
            </div>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="branch_price_<?= $branch['id'] ?>" placeholder="Price for Service <?= $branch['branch_location'] ?>" name="service_price[<?= $branch['id'] ?>]">
            </div>
           
        </div>
    <?php endforeach; ?>
</div>


                
                
                <input type="submit" name="submit" value="Add Service" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_product").length > 0) {
  $("#add_product").validate({
    rules: {
        category: {
        required: true,
      },
      sr_item: {
        required: true,
      },
      description: {
        required: true,
      },
      status: {
        required: true,
      },
      branch: {
        required: true,
      },
      service_price: {
        required: true,
      },
    }, 

    messages: {
        category
: {
        required: "   category  is required.",
      },
      sr_item: {
        required: "product name is required.",
      },

      description: {
        required: "description is required",
      },
      status: {
        required: "Please select Your status.",
      },
      branch: {
          required: "Please select Your branch.",
      },
      service_price: {
          required: "Service price is required.",
      }
    },
  });
}





  </script>
<?=$this->endsection();?>
