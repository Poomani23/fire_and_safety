<!-- Views/invoice/manage_invoice.php -->
<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Service Invoice</h6>
            <div class="text-right">
                <a href="<?php echo site_url('/add_service_invoice') ?>" class="btn btn-primary btn-icon-split">   
                    <span class="icon text-white-900">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">ADD</span>
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Service Invoice Id</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Mobile number</th>
                            <th>Address</th>
                            <th>Paid Amount</th>
                            <th>Status</th>   
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Loop through branches -->
                        <?php
                        $id = 1;
                        foreach ($sr_invoices as $sr_invoice):
                        ?>
                            <tr>
                                <td><?= $id;?></td>
                    <td><?= $sr_invoice['service_number'] ?></td>
                    <td><?= $sr_invoice['service_date'] ?></td>
                    <td><?= $sr_invoice['cr_name'] ?></td>
                    <td><?= $sr_invoice['mobile_no'] ?></td>
                    <td><?= $sr_invoice['cr_address'] ?></td>
                    <td><?= $sr_invoice['paid_amount'] ?></td>
                    <td><?= $sr_invoice['status'] ?></td>
                    <td>
                                    <a href="<?= base_url('edit_sr_invoice/'.$sr_invoice['id']) ?>"><i class="fas fa-edit" style="color:green;"></i></a>|
                                    <a href="<?= base_url('delete_sr_invoice/'.$sr_invoice['id']) ?>" ><i class="fa fa-trash" style="color:red;"></i></a>
                                    <a href="#" class="view-details" data-sr-invoice-id="<?= $sr_invoice['id'] ?>"><i class="fas fa-eye" style="color:blue;"></i></a>
                                     <a href="<?= site_url('srgeneratePdf/' . $sr_invoice['id']) ?>" target="_blank">
                                     <i class="fas fa-print" style="color:purple;"></i>
    </a>
                                </td>
                    </tr>
                            <?php $id++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Product Details -->
<div class="modal fade" id="srinvoiceDetailsModal" tabindex="-1" role="dialog" aria-labelledby="srinvoiceDetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="srinvoiceDetailsModalLabel"> ServiceInvoice Details</h5>
                <button type="button" class="close" aria-label="Close" id="closeModal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Content will be dynamically loaded here via AJAX -->
                <table class="table table-bordered" id="serviceTable">
                    <!-- Table for displaying product details -->
                    <thead>
                        <tr>
                            <th>S no</th>
                            <th>Service Item</th>
                            <th>Price</th>
                            
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
               <div class="text-right">
    <p id="grandTotal" class="mr-3"></p>
</div>
            </div>
        </div>
    </div>
</div>

<!-- Script to handle the click on the eye icon -->
<!-- Script to handle the click on the eye icon -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

<script>
    $(document).ready(function () {
        $('.view-details').on('click', function (e) {
            e.preventDefault();

            var srinvoiceId = $(this).data('sr-invoice-id');

            $.ajax({
                url: '<?= base_url('fetch_sr_invoice_details') ?>',
                type: 'POST',
                data: { srinvoiceId: srinvoiceId },
                dataType: 'json',
                success: function (response) {
                    if (response && response.service_item && response.service_item.length > 0) {
                        // Assuming you have a function to update the modal content
                        updatesrInvoiceDetailsModal(response);
                    } else {
                        // Handle the case where invoice_products array is empty or not present
                        console.error('Error fetching invoice details or empty invoice_products array.');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error or show a message if needed
                    console.error('Error fetching invoice details.', textStatus, errorThrown);
                }
            });
        });

        // Function to update the modal content
        function updatesrInvoiceDetailsModal(data) {
    console.log('Updating modal content:', data);

    $('#serviceTable tbody').empty();

    // Update the invoice product details table with the fetched data
    var grandTotal = 0;
    $.each(data.service_item, function (index, service) {
        console.log('Service:', service);
        
        // Use the price field for calculation
        var price = parseFloat(service.price);
        
        if (!isNaN(price)) {
            grandTotal += price;

            var row = '<tr>' +
                '<td>' + (index + 1) + '</td>' +
                '<td>' + service.sr_item + '</td>' +
                '<td>' + price.toFixed(2) + '</td>' +
                '</tr>';
            $('#serviceTable tbody').append(row);
        } else {
            console.error('Invalid price for service:', service);
        }
    });

    // Update the grand total
    console.log('Grand Total:', grandTotal);
    $('#grandTotal').text('Grand Total: ' + grandTotal.toFixed(2));

    // Show the modal
    $('#srinvoiceDetailsModal').modal('show');
}


        // Manually close the modal when the close button is clicked
        $('#closeModal').on('click', function () {
            $('#srinvoiceDetailsModal').modal('hide');
        });
    });
</script>


<?=$this->endsection();?>
