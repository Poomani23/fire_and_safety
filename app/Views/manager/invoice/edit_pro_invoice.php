<?=$this->extend('admin/manager')?>
<?= $this->section('content'); ?>
<style>
    .error {
        color: red !important;
        font-size: 15px !important;
        padding-left: 8px !important;
    }
</style>
<!-- Begin Page Content -->
<div class='container pt-5 pt-4'>
    <h1 class='text-center text-primary'> Edit Invoice </h1>
    <hr>

   <form class="user" method="post"  name="myForm" action="<?= route_to('update_invoice') ?>">


        <input type="hidden" name="invoice_id" value="<?= $invoice['id'] ?>">

        <!-- Invoice Details -->
        <div class='row'>
            <div class='col-md-6'>
                <h5 class='text-success'>Invoice Details</h5>
                <div class='form-group'>
                    <label for="invoice_no">Invoice Number</label>
                    <input type='text' name='invoice_no' id="invoice_no" required class='form-control' value="<?= esc($invoice['invoice_number']) ?>" readonly>
                </div>

                <div class='form-group'>
                    <label for="invoice_date">Invoice Date</label>
                    <input type='text' name='invoice_date' id="invoice_date" class='form-control' onfocus="(this.type='date')" value="<?= esc($invoice['invoice_date']) ?>">
                </div>
                <div class='form-group'>
                    <label for="cnumber">Mobile No</label>
                    <input type='text' name='cnumber' id="cnumber" class='form-control' value="<?= esc($invoice['mobile_no']) ?>">
                </div>
            </div>
            <div class='col-md-6'>
                <h5 class='text-success'>Customer Details</h5>
                <div class='form-group'>
                    <label for="cname">Name</label>
                    <input type='text' name='cr_name' id="cname" class='form-control' value="<?= esc($invoice['name']) ?>">
                </div>
                <div class='form-group'>
                    <label for="caddress">Address</label>
                    <input type='text' name='caddress' id="caddress" class='form-control' value="<?= esc($invoice['address']) ?>">
                </div>
                <div class='form-group'>
                    <label for="payment_type">Payment Type</label>
                    <select name='payment_type' id="payment_type" class='form-control'>
                        <option value="">Payment_type</option>
                        <option value="cash" <?= ($invoice['payment_type'] == 'cash') ? 'selected' : '' ?>>Cash</option>
                        <option value="gpay" <?= ($invoice['payment_type'] == 'gpay') ? 'selected' : '' ?>>Google pay</option>
                        <option value="phnepe" <?= ($invoice['payment_type'] == 'phnepe') ? 'selected' : '' ?>>Phone Pe</option>
                        <option value="paytm" <?= ($invoice['payment_type'] == 'paytm') ? 'selected' : '' ?>>Paytm</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- Product Details -->
        <div class='row'>
            <div class='col-md-6'>
                <label for="paid_amount">Paid Amount</label>
                <input type='text' name='paid_amount' id="paid_amount" class='form-control' value="<?= esc($invoice['paid_amount']) ?>">
            </div>

            <div class='col-md-6'>
                <label for="status">Status</label>
                <select name='status' id="status" class='form-control'>
                    <option value="">Status</option>
                    <option value="paid" <?= ($invoice['status'] == 'paid') ? 'selected' : '' ?>>Paid</option>
                    <option value="not_paid" <?= ($invoice['status'] == 'not_paid') ? 'selected' : '' ?>>Not Paid</option>
                </select>   
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-4">Update Invoice</button>
    </form>
</div>

<?= $this->endsection('content'); ?>
