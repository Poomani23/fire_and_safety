<!-- Views/invoice/ -->

<div class="container">
    <h4>Invoice Details</h4>
    <p><strong>Invoice Number:</strong> <?= $invoice['invoice']['invoice_number'] ?></p>
    <p><strong>Invoice Date:</strong> <?= $invoice['invoice']['invoice_date'] ?></p>
    <p><strong>Customer Name:</strong> <?= $invoice['invoice']['name'] ?></p>
    <!-- Add more details as needed -->

    <hr>

    <h4>Products</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><?= $product['product_name'] ?></td>
                    <td><?= $product['quantity'] ?></td>
                    <td><?= $product['price'] ?></td>
                    <td><?= $product['total'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
