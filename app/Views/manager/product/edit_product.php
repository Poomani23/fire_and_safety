<!-- edit_product.php (View) -->

<?=$this->extend('admin/admin')?>
<?=$this->section('content');?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="text-right">
        <a href="<?= base_url('manage_product') ?>" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-900">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Branch</h1>
            </div>
            <form method="post" action="<?= base_url('update/'.$product['id']) ?>" name="edit_product_form" id="edit_product_form" enctype="multipart/form-data">
            <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="product_id" class="form-label">Product ID:</label>
                    <input type="text" name="product_id" id="product_id" class="form-control" value="<?= $product['product_id'] ?>" readonly>
                    </div>
                    <div class="col-sm-6">
                <label for="product_name" class="form-label">Product Name:</label>
                <input type="text" name="product_name" id="product_name" class="form-control" value="<?= $product['product_name'] ?>" required>
                </div>
            </div>



    <!-- Display product image -->
            <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="product_image" class="form-label">Product Image:</label><br>
                     <img id="preview_image" src="<?= base_url('public/uploads/' . $product['product_image']) ?>" alt="Product Image" width="100" height="100">
                    </div> 
                    <div class="col-sm-6">
                
                <input type="file" class="form-control" id="product_image" placeholder="Product Image" name="product_image" onchange="previewImage(event)">
                      </div>
            </div>

            <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <label for="category" class="form-label">Category:</label>
        <select name="category" id="category" class="form-control" required>
            <option value="">Select Category</option>
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category['category_name']; ?>" <?php if ($product['category'] == $category['category_name']) echo 'selected'; ?>>
                    <?= $category['category_name']; ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>


            <div class="col-sm-6">
            <label for="status" class="form-label">Status:</label>
                <select name="status" id="status" class="form-select form-control" required>
                    <option value="available" <?= ($product['status'] === 'available') ? 'selected' : '' ?>>Available</option>
                    <option value="unavailable" <?= ($product['status'] === 'unavailable') ? 'selected' : '' ?>>Unavailable</option>
                </select>
              
            </div>
            </div>

            <div class="form-group row">
                    <div class="col-sm-12">
            <label for="description" class="form-label">Description:</label>
                <textarea name="description" id="description" class="form-control" required><?= $product['description'] ?></textarea>
            </div>
          </div>

            <!-- Display branches with prices (read-only) -->
            <div class="mb-3">
    <div class="row">
        <?php foreach ($pricesData as $price): ?>
            <?php
            $branchId = $price['branch_id'];
            $branchLocation = null;

            foreach ($branchLocations as $branch) {
                if ($branch['id'] == $branchId) {
                    $branchLocation = $branch['branch_location'];
                    break;
                }
            }
            ?>
            <div class="col-sm-4 mb-4 mb-sm-0 mt-3">
                <label for="branch_<?= $branchId ?>" class="form-label">Branch:</label>
                <input type="text" id="branch_<?= $branchId ?>" class="form-control" value="<?= $branchLocation ?? 'Branch Location Not Available' ?>" readonly>
                <label for="price_<?= $branchId ?>" class="form-label">Price:</label>
                <input type="text" id="price_<?= $branchId ?>" name="price_<?= $branchId ?>" class="form-control" value="<?= $price['price'] ?>">
                <label for="quantity_<?= $branchId ?>" class="form-label">Quantity:</label>
                <input type="text" id="quantity_<?= $branchId ?>" name="quantity_<?= $branchId ?>" class="form-control" value="<?= $price['quantity'] ?>">
            </div>
        <?php endforeach; ?>
    </div>
</div>



    <button type="submit" class="btn btn-primary">Update Product</button>
</form>

    </div>
    </div>
</div>
<!-- Include jQuery and jQuery Validate scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

<script>
    // Client-side form validation using jQuery Validate plugin
    if ($("#edit_product_form").length > 0) {
        $("#edit_product_form").validate({
            rules: {
                product_name: {
                    required: true,
                },
                category: {
                    required: true,
                },
                description: {
                    required: true,
                },
                status: {
                    required: true,
                },
            },
            messages: {
                product_name: {
                    required: "Product Name is required.",
                },
                category: {
                    required: "Category is required.",
                },
                description: {
                    required: "Description is required.",
                },
                status: {
                    required: "Please select the status.",
                },
            },
        });
    }
   
    function previewImage(event) {
        var reader = new FileReader();
        reader.onload = function () {
            var preview = document.getElementById('preview_image');
            preview.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    }

   
    

</script>

<?=$this->endsection();?>
