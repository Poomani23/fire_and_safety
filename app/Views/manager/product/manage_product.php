<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

     <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Products</h1>
                   
                    <!-- DataTales Example -->
                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Manage Products</h6>
                            <div class="text-right">
                           
                          
                        <a href="<?php echo site_url('add_product') ?>" class="btn btn-primary btn-icon-split">   
                <span class="icon text-white-900">
                    <i class="fa fa-plus"></i>
                </span>
                <span class="text">ADD</span>
            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <!-- Table Header -->
        <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Product Id</th>
                        <th>Product Image</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Price and Branch Location</th>
                        <th>Status</th>
                    </tr>
                </thead>
        <tbody>
            <!-- Loop through branches -->
             <?php 
                $id = 1;
                foreach ($products as $product): 
             ?>
                <tr>
                    <td><?= $id;?></td>
                    <td><?= $product['product_id'] ?></td>
                    <td><img src="<?= base_url('public/uploads/' . $product['product_image']) ?>" alt="Product Image" width="100" height="100"></td>
                            <td><?= $product['product_name'] ?></td>
                            <td><?= $product['category'] ?></td>
                            <td><?= $product['description'] ?></td>
                            <td>
                           <!-- Loop through products -->

    
    <!-- Get prices for the current product -->
    <?php $productPrices = $pricesData[$product['id']] ?? []; ?>

<!-- Loop through prices for branches -->
<?php foreach ($productPrices as $price): ?>
    <?php
    // Get branch location for the current branch_id
    $branchId = $price['branch_id'];
    $branchLocation = null;
    $quantity = $price['quantity'] ?? 'Quantity Not Available'; // Retrieve quantity information

    // Find the branch location from $branchLocations using branch_id
    foreach ($branchLocations as $branch) {
        if ($branch['id'] == $branchId) {
            $branchLocation = $branch['branch_location'];
            break;
        }
    }
    ?>
    <p>
        Branch: <?= $branchLocation ?? 'Branch Location Not Available' ?> | Price: <?= $price['price'] ?> | Quantity: <?= $quantity ?>
    </p>
<?php endforeach; ?>


                            </td>


                            <td>
        <!-- Edit product link -->
        <a href="<?= site_url('edit_product/' . $product['id']) ?>"><i class="fas fa-edit"></i></a> |
        
        <!-- Delete product link -->
        <a href="<?= site_url('delete_product/' . $product['id']) ?>" ><i class="fa fa-trash"></i></a>
    </td>
                </tr>
                <?php $id++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div></div>


    <!-- Page level plugins -->
   
<?=$this->endsection();?>
