<?= $this->extend('admin/manager') ?>

<?= $this->section('content') ?>

<style>
    .error {
        color: red !important;
        font-size: 15px !important;
        padding-left: 8px !important;
    }

    .preview-container {
        display: flex;
        align-items: center;
        margin-top: 10px;
    }

    .preview-image {
        max-width: 100%;
        max-height: 150px;
        margin-left: 10px;
    }
</style>

<?php 
    $shop_name = !empty($shop_name) ? $shop_name : '';
    $invoice_prefix = !empty($invoice_prefix) ? $invoice_prefix : '';
    $profile_img = !empty($profile_img) ? $profile_img : '';
    $login_img = !empty($login_img) ? $login_img : '';
    $reg_img = !empty($reg_img) ? $reg_img : '';
?>

<div class="container-fluid">
    <div class="text-right">
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Settings edit details</h1>
            </div>
            <form action="manager/update" method="post" class="user" enctype="multipart/form-data">
                <!-- Shop Name Field -->
                <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Shop Name:</label>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="exampleFirstName" placeholder="Shop Name" name="shop_name" value="<?= $row['shop_name'] ?>">
                    </div>
                </div>

                <!-- Image Fields -->
                <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Profile Image:</label>
                        <input type="file" id="profile_img" name="profile_img" class="form-control" placeholder="Profile Image">
                    </div>
                    <div class="col-sm-5">
                        <div class="preview-container">
                            <img id="profilePreview" class="preview-image" src="<?= base_url('public/uploads/' . $row['profile_img']) ?>" alt="">
                        </div>
                    </div>
                </div>

                <!-- <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Invoice Prefix:</label>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="exampleFirstName" placeholder="Invoice Code" name="invoice_prefix" value="<?= $row['invoice_prefix'] ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Login Image:</label>
                        <input type="file" id="login_img" name="login_img" class="form-control" placeholder="Login Image">
                    </div>
                    <div class="col-sm-5">
                        <div class="preview-container">
                            <img id="loginPreview" class="preview-image" src="<?= base_url('public/uploads/' . $row['login_img'] ) ?>" alt="">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Register Image:</label>
                        <input type="file" id="reg_img" name="reg_img" class="form-control" placeholder="Register Image">
                    </div>
                    <div class="col-sm-5">
                        <div class="preview-container">
                            <img id="regPreview" class="preview-image" src="<?= base_url('public/uploads/' . $row['reg_img']) ?>" alt="">
                        </div>
                    </div>
                </div> -->

                <!-- Submit Button -->
                <div class="form-group row text-center">
                    <div class="col-sm-10">
                        <input type="submit" name="submit" value="<?= count($row) > 0 ? 'Update' : 'Submit'; ?>" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- JavaScript to handle file input change event and update image previews -->
<!-- JavaScript to handle file input change event and update image previews -->
<script>
    function previewImage(input, imageId) {
        const fileInput = input;
        const previewImage = document.getElementById(imageId);

        fileInput.addEventListener('change', function () {
            const file = this.files[0];

            if (file) {
                const reader = new FileReader();

                reader.onload = function (e) {
                    previewImage.src = e.target.result;
                };

                reader.readAsDataURL(file);
            } else {
                console.log('No file chosen.');
                previewImage.src = '<?= base_url('uploads/') ?>' + (file ? file.name : 'placeholder-image.jpg');
 // Adjust the path accordingly
            }
        });
    }

    // Call the function for each file input
    previewImage(document.getElementById('profile_img'), 'profilePreview');
    previewImage(document.getElementById('login_img'), 'loginPreview');
    previewImage(document.getElementById('reg_img'), 'regPreview');
</script>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

<?= $this->endsection() ?>
