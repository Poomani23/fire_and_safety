<!-- invoice_pdf.php -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .header {
            text-align: center;
        }

        .header h2 {
            margin: 0;
        }

        .header p {
            margin: 5px 0;
        }

        .invoice-info {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
            padding: 10px;
            /* Border under the invoice info */
        }

        .bill-to {
            margin-bottom: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }

        .total-section {
            text-align: right;
            /* Border above the total */
            padding-top: 10px;
        }

        .signature {
            margin-top: 120px;
            /* Border above the signature */
            padding-top: 10px;
            text-align: right;
        }

        .amount_in_words {
            text-align: left;
        }

       .invoice_number {
        text-align: right;
       }
    </style>
</head>

<body>
    <div class="header">
        <h2>Fire and Safety</h2>
        <p><?= $customerDetails['branch'] ?></p>
    </div>
     <div class="invoice_number">
            <h4>Invoice:</h4>
            <p>Invoice No: <?= $invoice['invoice_number'] ?></p>
            <p>Invoice Date: <?= $invoice['invoice_date'] ?></p>
        </div>

    <hr>

    <div class="customer-invoice-container">
        <div class="bill-to">
            <h4>Bill To:</h4>
            <p><?= $customerDetails['name'] ?></p>
            <p><?= $customerDetails['address'] ?></p>
        </div>

       
    </div>

    <table>
        <thead>
            <tr>
                <th>PRODUCT NAME</th>
                <th>PRICE</th>
                <th>QTY</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($invoices as $product) : ?>
                <tr>
                    <td><?= $product['product_name'] ?></td>
                    <td><?= $product['price'] ?></td>
                    <td><?= $product['quantity'] ?></td>
                    <td><?= $product['total'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="total-section">
        <p><strong>TOTAL:</strong> <?= $grandTotal ?></p>
        <p class="amount_in_words">Amount in Words: <?= convertToWords($grandTotal) ?></p>
    </div>

    <div class="signature">
        <p>Authorized Signature</p>
    </div>
</body>

</html>
