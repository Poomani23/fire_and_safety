<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
    
</style>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="text-right">
        <a href="<?= base_url('manage_product') ?>" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-900">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Product</h1>
            </div>
            <form method="post" action="<?= base_url('save_product'); ?>" class="user" name="add_product" id="add_product" enctype="multipart/form-data">
            <div class="form-group row">
            <div class="col-sm-6">
    <select class="form-control" id="category" name="category">
        <option value="">Select Category</option>
        <?php foreach ($categories as $category): ?>
            <option value="<?= $category['category_name']; ?>"><?= $category['category_name']; ?></option>
        <?php endforeach; ?>
    </select>
</div>



                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="product_name" placeholder="Product Name" name="product_name">
                    </div>
                </div>
                <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <label for="product_image" class="form-label">Product Image:</label><br>
        <img id="preview_image" src="" alt="No image selected" width="100" height="100">
    </div>
                   
            </div>
                <div class="form-group row">
                <div class="col-sm-6">
                
                <input type="file" class="form-control" id="product_image" placeholder="Product Image" name="product_image" onchange="previewImage(event)">
                      </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <select name="status" id="status" class="form-select form-control" required>
                            <option value="">Select Status</option>
                            <option value="available">Available</option>
                            <option value="Unavailable">Unavailable</option>
                        </select>
                    </div>
                </div>
                <div class ="form-group row">
                <div class="col-sm-12">
                       <textarea class="form-control" id="description" placeholder="Description" name="description"></textarea> 
                    </div>
                    </div>
                   
                   
                    <div class="form-group">
    <?php foreach ($branches as $branch): ?>
        <div class="row mb-3">
            <div class="col-sm-2">
                <label for="branch_<?= $branch['id'] ?>" class="mr-2"><?= $branch['branch_location'] ?>:</label>
            </div>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="branch_price_<?= $branch['id'] ?>" placeholder="Price for <?= $branch['branch_location'] ?>" name="product_price[<?= $branch['id'] ?>]">
            </div>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="branch_quantity_<?= $branch['id'] ?>" placeholder="Quantity for <?= $branch['branch_location'] ?>" name="product_quantity[<?= $branch['id'] ?>]">
            </div>
        </div>
    <?php endforeach; ?>
</div>


                
                
                <input type="submit" name="submit" value="Add Product" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_product").length > 0) {
  $("#add_product").validate({
    rules: {
        category: {
        required: true,
      },
      product_name: {
        required: true,
      },
      product_image: {
        required: true,
      },
      description: {
        required: true,
      },
      status: {
        required: true,
      },
      branch: {
        required: true,
      },
      product_price: {
        required: true,
      },
    }, 

    messages: {
        category
: {
        required: "   category  is required.",
      },
      product_name: {
        required: "product name is required.",
      },
      product_image: {
        required: "Please select Your status.",
      },
      description: {
        required: "description is required",
      },
      status: {
        required: "Please select Your status.",
      },
      branch: {
          required: "Please select Your branch.",
      },
      product_price: {
          required: "Product price is required.",
      }
    },
  });
}
function previewImage(event) {
    var reader = new FileReader();
    var image = document.getElementById('preview_image');
    var file = event.target.files[0]; // Get the selected file
    
    if (file) {
        // Check if the selected file is an image
        if (file.type.match('image.*')) {
            reader.onload = function(){
                image.src = reader.result;
            };
    
            reader.readAsDataURL(file);
        } else {
            // Display 'No image selected' text if the selected file is not an image
            image.src = '';
            image.alt = 'No image selected';
        }
    } else {
        // Display 'No image selected' text if no file is chosen
        image.src = '';
        image.alt = 'No image selected';
    }
}




  </script>
<?=$this->endsection();?>
