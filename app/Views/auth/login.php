<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Login</title>
<base href="/">
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
        // Check for success flash message flag
        var successMessage = "<?php echo session()->getFlashdata('success'); ?>";

        // If success message exists, display an alert
        if (successMessage) {
            alert(successMessage);
        }
    </script>
</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                        <div class="col-lg-6 d-none d-lg-block ">
                    <?php if (!empty($loginImgURL)) : ?>
                        <img src="<?= esc($loginImgURL); ?>" alt="Login Image" height="480px" width="460px">
                    <?php else : ?>
                        <!-- Fallback image URL -->
                        <img src="https://source.unsplash.com/K4mSJ7kc0As/600x800" alt="Fallback Image">
                    <?php endif; ?>
                </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <?php

$failMessage = session()->getFlashData('fail');
?>
<?php if (!empty($failMessage)) : ?>
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong><?= $failMessage?></strong> 
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<?php endif; ?>
<?php
$error = isset($error) ? $error : '';
if (!empty($error)) :
?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong><?= $error ?></strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<?php endif; ?>
                                   <form class="user" method="post" action="<?= base_url('login') ?>" autocomplete="off">
                                <?= csrf_field(); ?>

                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Username " name="username" value="<?= set_value('username');?>">
                                                <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'username'):'' ?></span>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password" name="password"value="<?= set_value('password');?>">
                                                <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'password'):'' ?></span>
                                        </div>
                                       <button type="submit" class="btn btn-primary btn-user btn-block">
        Login
    </button>
                                        <hr>
                                       
                                    </form>
                                    <div class="text-center">
                                        <a class="small" href="<?= base_url('register') ?>">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>