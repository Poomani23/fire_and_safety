<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Register</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block ">
                    <?php if (!empty($regImgURL)) : ?>
                        <img src="<?= esc($regImgURL); ?>" alt="Register Image" height="500px" width="500px">
                    <?php else : ?>
                        <!-- Fallback image URL -->
                        <img src="https://source.unsplash.com/Mv9hjnEUHR4/600x800" alt="Fallback Image">
                    <?php endif; ?>
                </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                           <?php
$failMessage = session()->getFlashData('fail');
?>


    
<?php if (!empty($failMessage)) : ?>
    <div class="alert alert-danger alert-dismissible fade show">
        <?= $failMessage ?>
    </div>
<?php endif; ?>



                           <form class="user" action="<?= base_url('register') ?>" method="post">

                                  <?= csrf_field(); ?>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                            placeholder="First Name" name="firstname" value="<?= set_value('firstname');?>">
                                            <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'firstname'):'' ?></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                                            placeholder="Last Name" name="lastname" value="<?= set_value('lastname');?>">
                                             <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'lastname') :'' ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                        placeholder="Email Address" name="email" value="<?= set_value('email');?>">
                                         <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'email'):'' ?></span>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" name="password" value="<?= set_value('password');?>">
                                             <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'password'):'' ?></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user"
                                            id="exampleRepeatPassword" placeholder="Repeat Password" name="confirm_password" value="<?= set_value('confirm_password');?>">
                                             <span class="text-danger text-sm"><?=isset($validation)?display_form_errors($validation,'confirm_password'):'' ?></span>
                                    </div>
                                    <input type="hidden" name="status" value="active">
                                </div>
                                <button class="btn btn-primary btn-user btn-block"> Register Account</button>
                            </form>
                            
                            <div class="text-center">
                                <a class="small" href="<?=base_url('login')?>">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>