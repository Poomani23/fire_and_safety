<!-- edit_category.php (View) -->

<?=$this->extend('admin/admin')?>
<?=$this->section('content');?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>

<div class="container mt-4">
    <h2>Edit Category</h2>
    <form method="post" action="<?= base_url('category/update/'.$category['id']) ?>" name="add_create" id="add_create">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" name="category_name" id="category_name" class="form-control" value="<?= $category['category_name'] ?>" required>
                    </div>
                   <div class="col-sm-6">
                   <select name="status" id="status" class="form-select form-control" required>
                <option value="available" <?= ($category['status'] === 'available') ? 'selected' : '' ?>>Available</option>
                <option value="unavailable" <?= ($category['status'] === 'unavailable') ? 'selected' : '' ?>>Unavailable</option>
            </select>
                    </div> 
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <!-- <select name="status" id="status" class="form-select form-control" required>
                            <option value="">Select Status</option>
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select> -->
                    </div>
                </div>
                <input type="submit" name="submit" value="Add Category" class="btn btn-primary btn-block">
            </form>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_create").length > 0) {

   
  $("#add_create").validate({
    rules: {
        category_name: {
        required: true,
      },
      status: {
        required: true,
      },
    }, 

    messages: {
        category_name: {
        required: "branch  Name is required.",
      },
      status: {
        required: "Please select Your status.",
      },
    },
  });
}

  </script>
<?=$this->endsection();?>
