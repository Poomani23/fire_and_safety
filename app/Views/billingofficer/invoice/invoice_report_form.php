<?=$this->extend('admin/billingofficer')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="text-right">
        <a href="<?= base_url('billingofficer/invoice-report') ?>" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-900">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
    </div>

    <div class="col-lg-8" style="margin: auto;">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Select Date Range</h1>
            </div>
            <form method="post" action="<?= base_url('billingofficer/invoice-report'); ?>" class="user" name="add_create" id="invoice_report">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="start_date">Start Date:</label>
                    <input type="date" id="start_date" class="form-control" name="start_date" required>
                    </div>
                   <div class="col-sm-6">
                   <label for="end_date">End Date:</label>
                    <input type="date" id="end_date" class="form-control" name="end_date" required>
                    </div> 
                </div>
                <input type="submit" name="submit" value="Get Report" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#invoice_report").length > 0) {
  $("#invoice_report").validate({
    rules: {
        start_date: {
        required: true,
      },
        end_date: {
        required: true,
      },
    }, 

    messages: {
        start_date: {
        required: "Start Date is required.",
      },
        end_date: {
        required: "End Date is required.",
      },
    },
  });
}

  </script>
<?=$this->endsection();?>
