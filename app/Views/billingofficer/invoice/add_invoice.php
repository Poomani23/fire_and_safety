<?=$this->extend('admin/billingofficer')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
              <!-- Begin Page Content -->
              <div class='container pt-5 pt-4'>
            <h1 class='text-center text-primary'> Invoice </h1><hr>
            
            
              <form class="user" method="post" id="invoiceForm" name="myForm" action="<?= route_to('billingofficer/save_invoice') ?>">


                <!--  <form class="user"  method="post" id="enc_validate" name="myForm"> -->
                <div class='row'>
                    <div class='col-md-4'>
                        <h5 class='text-success'>Invoice Details</h5>
                        <div class='form-group'>
    <label>Invoice Number</label>
    <input type='text' name='invoice_no' id="invoice_no" required class='form-control' value="<?= isset($invoiceNumber) ? esc($invoiceNumber) : '' ?>">

</div>

                        <div class='form-group'>
                             <label>Invoice Date</label>
                            <input type='text' name='invoice_date' id="invoice_date" class='form-control' onfocus="(this.type='date')"> 
                        </div>
                        <div class='form-group'>
                            <label>Mobile No</label>
                            <input type='text' name='cnumber'  id="cnumber" class='form-control'>
                        </div>
                    </div> 
                    <div class='col-md-8'>
                        <h5 class='text-success'>Customer Details</h5>
                        <div class='form-group'>
                            <label>Name</label>
                            <input type='text' name='cr_name' id="cname" class='form-control'>
                        </div>
                        <div class='form-group'>
                            <label>Address</label>
                            <input type='text' name='caddress' id="caddress" class='form-control'>
                        </div>
                        <div class='form-group'>
                            <label>Branch</label>
                                          <select name='branch' id='branch' class='form-control'>
                      <option>Select Branch</option>
                      <?php foreach ($branches as $branch): ?>
                          <option value="<?= $branch['branch_location'] ?>"><?= $branch['branch_location'] ?></option>
                      <?php endforeach; ?>
                  </select>
                        </div>
                     
                    </div>
                </div>

               
                    
            
           
                <div class='row'>
    <div class='col-md-12'>
        <h5 class='text-success'>Product Details</h5>
        <table class='table table-bordered'>
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id='product_tbody'>
                <tr>
                    <td>
                        <select class="form-control"  id="product_name" name="pname[]">
                            <option value="">Product_name</option>
                        </select>
                    </td>
                    <td><input type='text'  name='price[]' class='form-control price pro_price  ' id='price'></td>
                    <td><input type='text'  name='qty[]' class='form-control qty' id='qty'></td>
                    <td><input type='text'  name='total[]' class='form-control total' id='total'></td>
                    <td><input type='button' value='x' class='btn btn-danger btn-sm btn-row-remove'> </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td><input type='button' value='+ Add Row' class='btn btn-primary btn-sm' id='btn-add-row'></td>
                    <td colspan='2' class='text-right'>Total</td>
                    <td><input type='text' name='grand_total' id='grand_total' class='form-control'  ></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class='row'> 
    <div class='col-md-3'>
        <label>Payment Type</label>
        <select name='payment_type' id="payment_type"  class='form-control'>
            <option value="">Payment_type</option>
            <option value="cash">Cash</option>
            <option value="gpay">Google pay</option>
            <option value="phnepe">Phone Pe</option>
            <option value="paytm">Paytm</option>
            <option value="amazonpay">Amazon pay</option>
        </select>
    </div>
    <div class='form-group paid col-md-3'>
        <label>Paid</label>
        <input type='text' name='paid_amount' id="paid" class='form-control pay'> 
    </div>
    <div class='form-group type col-md-3'>
        <label>Status </label>
        <select name='status'  id="status" class='form-control form'>
            <option value="paid">Paid</option>
            <option value="not_paid">Not_Paid</option>
        </select>
    </div>
</div>


<div class='col-md-12'>
    <div class='form-group d-flex justify-content-end'>
        <input type='submit' name='submit' value='Save Invoice' class='btn btn-primary mr-3'>
        <button type="submit" name="print" value="1" class="btn btn-primary ">Print Invoice</button>
                                            
    </div>
</div>



            </form>

        </div>
        <div>
        
    
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
  $(document).ready(function () {
    // Fetch product names based on branch selection
    $('#branch').change(function () {
      var branch_location = $(this).val();

      $.ajax({
        type: 'POST',
        url: '<?= base_url('getProductNamesByBranch') ?>',
        data: { branch_location: branch_location },
        dataType: 'json',
        success: function (response) {
          var productDropdown = $('#product_name');
          productDropdown.empty();

          // Add a default "Select Product" option
          productDropdown.append($('<option>', {
            value: '',
            text: 'Select Product'
          }));

          // Append product names
          $.each(response.product_names, function (index, product) {
            var productName = product.product_name;
            productDropdown.append($('<option>', {
              value: productName,
              text: productName
            }));
          });
        },
        error: function () {
          alert('Error fetching product names.');
        }
      });
    });

    // Fetch and display price when a product is selected
   
// Fetch and display price when a product is selected
$(document).on('change', '#product_name', function () {
  // Check if a product is selected
  var selectedProduct = $(this).val();
  var currentRow = $(this).closest('tr'); // Store the reference to the current row

  if (selectedProduct) {
    var branch_location = $('#branch').val();
    var product_name = selectedProduct;

    $.ajax({
      type: 'POST',
      url: '<?= base_url('getProductPrice') ?>',
      data: { branch_location: branch_location, product_name: product_name },
      dataType: 'json',
      success: function (response) {
        if (response.price) {
          // Display the price in the desired element of the current row
          currentRow.find('.price').val(response.price);
          updateTotal();
        } else {
          currentRow.find('.price').val('');
        }
      },
      error: function () {
        alert('Error fetching product price.');
      }
    });
  }
});


    // Update total when quantity changes
    $(document).on('input', '.qty', function () {
      updateTotal();
    });

    // Add row
    $('#btn-add-row').click(function () {
  var newRow = $('#product_tbody tr:last').clone();
  newRow.find('input').val('');

  // Modify the close button to include 'x'
  var closeButton = newRow.find('.btn-row-remove');
  closeButton.show().text('x');

  // If the button in the cloned row is empty, add 'x'
  if (!closeButton.val()) {
    closeButton.val('x');
  }

  $('#product_tbody').append(newRow);
});
    // Remove row
    $(document).on('click', '.btn-row-remove', function () {
  $(this).closest('tr').remove();
  updateTotal();
});

    // Function to update total based on quantity and price
    function updateTotal() {
      // Initialize grand total
      var grandTotal = 0;

      // Iterate over each row in the table body
      $('#product_tbody tr').each(function () {
        // Get the quantity and price for the current row
        var quantity = $(this).find('.qty').val();
        var price = $(this).find('.price').val();

        // Calculate total for the current row
        var total = quantity * price;

        // Update the total field for the current row
        $(this).find('.total').val(total.toFixed(2));

        // Add the total to the grand total
        grandTotal += total;
      });

      // Update the grand total field in the table footer
      $('#grand_total').val(grandTotal.toFixed(2));
    }

  

    
    
                
  });
</script>






<?=$this->endsection();?>
