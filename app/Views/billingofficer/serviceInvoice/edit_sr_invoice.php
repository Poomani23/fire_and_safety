<?=$this->extend('admin/billingofficer')?>
<?= $this->section('content'); ?>
<style>
    .error {
        color: red !important;
        font-size: 15px !important;
        padding-left: 8px !important;
    }
</style>
<!-- Begin Page Content -->
<div class='container pt-5 pt-4'>
    <h1 class='text-center text-primary'> Edit Invoice </h1>
    <hr>

   <form class="user" method="post"  name="myForm" id="myForm" action="<?= route_to('billingofficer/update_sr_invoice') ?>">


        <input type="hidden" name="sr_invoice_id" value="<?=  $serviceinvoice['id'] ?>">

        <!-- Invoice Details -->
        <div class='row'>
            <div class='col-md-6'>
                <h5 class='text-success'> Service Invoice Details</h5>
                <div class='form-group'>
                    <label for="service_number">Invoice Number</label>
                    <input type='text' name='service_number' id="service_number" required class='form-control' value="<?= esc($serviceinvoice['service_number']) ?>" readonly>
                </div>

                <div class='form-group'>
                    <label for="service_date">Service Date</label>
                    <input type='text' name='service_date' id="service_date" class='form-control' onfocus="(this.type='date')" value="<?= esc($serviceinvoice['service_date']) ?>">
                </div>
                <div class='form-group'>
                    <label for="mobile_no">Mobile No</label>
                    <input type='text' name='mobile_no' id="mobile_no" class='form-control' value="<?= esc($serviceinvoice['mobile_no']) ?>">
                </div>
            </div>
            <div class='col-md-6'>
                <h5 class='text-success'>Customer Details</h5>
                <div class='form-group'>
                    <label for="cname">Name</label>
                    <input type='text' name='cr_name' id="cname" class='form-control' value="<?= esc($serviceinvoice['cr_name']) ?>">
                </div>
                <div class='form-group'>
                    <label for="caddress">Address</label>
                    <input type='text' name='cr_address' id="caddress" class='form-control' value="<?= esc($serviceinvoice['cr_address']) ?>">
                </div>
                <div class='form-group'>
                    <label for="payment_type">Payment Type</label>
                    <select name='payment_type' id="payment_type" class='form-control'>
                        <option value="">Payment_type</option>
                        <option value="cash" <?= ($serviceinvoice['payment_type'] == 'cash') ? 'selected' : '' ?>>Cash</option>
                        <option value="gpay" <?= ($serviceinvoice['payment_type'] == 'gpay') ? 'selected' : '' ?>>Google pay</option>
                        <option value="phnepe" <?= ($serviceinvoice['payment_type'] == 'phnepe') ? 'selected' : '' ?>>Phone Pe</option>
                        <option value="paytm" <?= ($serviceinvoice['payment_type'] == 'paytm') ? 'selected' : '' ?>>Paytm</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- Product Details -->
        <div class='row'>
            <div class='col-md-6'>
                <label for="paid_amount">Paid Amount</label>
                <input type='text' name='paid_amount' id="paid_amount" class='form-control' value="<?= esc($serviceinvoice['paid_amount']) ?>">
            </div>

            <div class='col-md-6'>
                <label for="status">Status</label>
                <select name='status' id="status" class='form-control'>
                    <option value="">Status</option>
                    <option value="paid" <?= ($serviceinvoice['status'] == 'paid') ? 'selected' : '' ?>>Paid</option>
                    <option value="not_paid" <?= ($serviceinvoice['status'] == 'not_paid') ? 'selected' : '' ?>>Not Paid</option>
                </select>   
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-4">Update Invoice</button>
    </form>
</div>

<?= $this->endsection('content'); ?>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<Script>
     if ($("#myForm").length > 0) {
  $("#myForm").validate({
        rules: {
            service_number: {
                required: true
            },
            service_date: {
                required: true
            },
            mobile_no: {
                required: true
            },
            cr_name: {
                required: true
            },
            cr_address: {
                required: true
            },
            branch: {
                required: true
            },
            // Add more rules as per your form fields
        },
        messages: {
            service_number: {
                required: "Invoice number is required"
            },
            service_date: {
                required: "Invoice date is required"
            },
            mobile_no: {
                required: "Mobile number is required"
            },
            cr_name: {
                required: "Name is required"
            },
            cr_address: {
                required: "Address is required"
            },
            branch: {
                required: "Branch is required"
            },
            // Add more messages as per your form fields
        },
    });
}
</Script>