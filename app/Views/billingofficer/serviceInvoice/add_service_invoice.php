<?=$this->extend('admin/billingofficer')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
    }
</style>
<div class='container pt-5 pt-4'>
    <h1 class='text-center text-primary'>Add Service Invoice</h1><hr>
    <form class="user" method="post" id="serviceInvoiceForm"  name ="serviceInvoiceForm" action="<?= route_to('billingofficer/save_service_invoice') ?>">
        <div class='row'>
        
            <div class='col-md-4'>
                <h5 class='text-success'>Invoice Details</h5>
                <div class='form-group'>
                    <label>Invoice Number</label>
                    
                        <input type='text' name='service_number' id="service_number" required class='form-control' value="<?= isset($serviceNumber) ? esc($serviceNumber) : '' ?>">

                </div>
                <div class='form-group'>
                    <label>Invoice Date</label>
                    <input type='text' name='service_date' id="service_date" class='form-control' onfocus="(this.type='date')">
                </div>
                <div class='form-group'>
                    <label>Mobile No</label>
                    <input type='text' name='mobile_no'  id="cnumber" class='form-control'>
                </div>
            </div>
            <div class='col-md-8'>
                <h5 class='text-success'>Customer Details</h5>
                <div class='form-group'>
                    <label>Name</label>
                    <input type='text' name='cr_name' id="cname" class='form-control'>
                </div>
                <div class='form-group'>
                    <label>Address</label>
                    <input type='text' name='cr_address' id="caddress" class='form-control'>
                </div>
                <div class='form-group'>
                    <label>Branch</label>
                    <select name='branch' id='branch' class='form-control'>
                        <option>Select Branch</option>
                        <?php foreach ($branches as $branch): ?>
                            <option value="<?= $branch['branch_location'] ?>"><?= $branch['branch_location'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-12'>
                <h5 class='text-success'>Service Details</h5>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th>Service</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id='service_tbody'>
                        <tr>
                            <td>
                        <select class="form-control"  id="sr_item" name="sr_item[]">
                            <option value="">Service Item</option>
                        </select>
                    </td>
                            <td><input type='text'  name='price[]' class='form-control price service_price' id='price'></td>
                            <td><input type='button' value='x' class='btn btn-danger btn-sm btn-row-remove'> </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><input type='button' value='+ Add Row' class='btn btn-primary btn-sm' id='btn-add-row'></td>
                            
                            <td><input type='text' name='grand_total' id='grand_total' class='form-control'  ></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class='row'> 
            <div class='col-md-3'>
                <label>Payment Type</label>
                <select name='payment_type' id="payment_type"  class='form-control'>
                    <option value="">Payment Type</option>
                    <option value="cash">Cash</option>
                    <option value="gpay">Google pay</option>
                    <option value="phnepe">Phone Pe</option>
                    <option value="paytm">Paytm</option>
                    <option value="amazonpay">Amazon pay</option>
                </select>
            </div>
            <div class='form-group paid col-md-3'>
                <label>Paid</label>
                <input type='text' name='paid_amount' id="paid" class='form-control pay'> 
            </div>
            <div class='form-group type col-md-3'>
                <label>Status </label>
                <select name='status'  id="status" class='form-control form'>
                    <option value="paid">Paid</option>
                    <option value="not_paid">Not Paid</option>
                </select>
            </div>
        </div>

        <div class='col-md-12'>
            <div class='form-group d-flex justify-content-end'>
                <input type='submit' name='submit' value='Save Service Invoice' class='btn btn-primary mr-3'>
                <button type="submit" name="srprint" value="1" class="btn btn-primary ">Print Invoice</button>

            </div>
        </div>
    </form>
</div>

 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
  $(document).ready(function () {


    
    // Fetch service items based on branch selection
// Fetch service items based on branch selection
 $('#branch').change(function () {
    var branch_location = $(this).val();

    $.ajax({
      type: 'POST',
      url: '<?= base_url("getServiceItemsByBranch") ?>' ,// Updated URL
    data: { branch_location: branch_location },
      success: function (response) {
        var productDropdown = $('#sr_item');
        productDropdown.empty();

        // Add a default "Select Service" option
        productDropdown.append($('<option>', {
          value: '',
          text: 'Select Service'
        }));

        // Append service items
        $.each(response.service_items, function (index, service) {
          // Use 'service' instead of 'serviceItem'
          productDropdown.append($('<option>', {
            value: service.sr_item,
            text: service.sr_item
          }));
        });
      },
      error: function () {
        alert('Error fetching service items.');
      }
    });
  });



   
// Fetch and display price when a service is selected
function updateGrandTotal() {
        var total = 0;

        // Loop through each row in the table
        $('#service_tbody tr').each(function () {
            var price = parseFloat($(this).find('.price').val()) || 0;
            total += price;
        });

        // Update the grand total input field
        $('#grand_total').val(total.toFixed(2));
    }

    // Event handler for changing service items
    $(document).on('change', '#sr_item', function () {
        // Check if a service is selected
        var selectedService = $(this).val();
        var currentRow = $(this).closest('tr');

        if (selectedService) {
            var branch_location = $('#branch').val();
            var sr_item = selectedService;

            $.ajax({
                type: 'POST',
                url: '<?= base_url('getServicePrice') ?>',
                data: { branch_location: branch_location, sr_item: sr_item },
                dataType: 'json',
                success: function (response) {
                    if (response.price) {
                        currentRow.find('.price').val(response.price);
                    } else {
                        currentRow.find('.price').val('');
                    }

                    // Update the grand total after changing the service price
                    updateGrandTotal();
                },
                error: function () {
                    alert('Error fetching service price.');
                }
            });
        }
    });

    // Event handler for changing prices
    $(document).on('change', '.price', function () {
        updateGrandTotal();
    });

    // Event handler for adding a new row
    $('#btn-add-row').click(function () {
        var newRow = $('#service_tbody tr:last').clone();
        newRow.find('input').val('');

        // Modify the close button to include 'x'
        var closeButton = newRow.find('.btn-row-remove');
        closeButton.show().text('x');

        // If the button in the cloned row is empty, add 'x'
        if (!closeButton.val()) {
            closeButton.val('x');
        }

        $('#service_tbody').append(newRow);

        // Update the grand total after adding a new row
        updateGrandTotal();
    });

    // Event handler for removing a row
    $(document).on('click', '.btn-row-remove', function () {
        $(this).closest('tr').remove();
        updateGrandTotal();
    });

  

   
                
  });




</script>
<Script>
     if ($("#serviceInvoiceForm").length > 0) {
  $("#serviceInvoiceForm").validate({
        rules: {
            service_number: {
                required: true
            },
            service_date: {
                required: true
            },
            mobile_no: {
                required: true
            },
            cr_name: {
                required: true
            },
            cr_address: {
                required: true
            },
            branch: {
                required: true
            },
            // Add more rules as per your form fields
        },
        messages: {
            service_number: {
                required: "Invoice number is required"
            },
            service_date: {
                required: "Invoice date is required"
            },
            mobile_no: {
                required: "Mobile number is required"
            },
            cr_name: {
                required: "Name is required"
            },
            cr_address: {
                required: "Address is required"
            },
            branch: {
                required: "Branch is required"
            },
            // Add more messages as per your form fields
        },
    });
}
</Script>



<?=$this->endsection();?>
