<!-- Views/invoice/manage_invoice.php -->
<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Invoice</h6>
            <div class="text-right">
                <a href="<?php echo site_url('/add_invoice') ?>" class="btn btn-primary btn-icon-split">   
                    <span class="icon text-white-900">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">ADD</span>
                </a>
            </div>
        </div>
        <div class="card-body">
            <!-- Display the selected date range -->
<h2>Invoice Report - <?= $startDate ?> to <?= $endDate ?></h2>

<!-- Display individual invoices -->
<table>
    <thead>
        <tr>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Paid Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($invoices as $invoice) : ?>
            <tr>
                <td><?= $invoice['invoice_number'] ?></td>
                <td><?= $invoice['invoice_date'] ?></td>
                <td><?= $invoice['paid_amount'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<!-- Display the overall total -->
<p><strong>Overall Total Paid Amount:</strong> <?= $overallTotal ?></p>

        </div>
    </div>
</div>

<!-- Modal for Product Details -->

            </div>
        </div>
    </div>
</div>

<!-- Script to handle the click on the eye icon -->
<!-- Script to handle the click on the eye icon -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>




<?=$this->endsection();?>
