<!-- Views/invoice/manage_invoice.php -->
<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Invoice</h6>
            <div class="text-right">
                <a href="<?php echo site_url('/add_invoice') ?>" class="btn btn-primary btn-icon-split">   
                    <span class="icon text-white-900">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">ADD</span>
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Invoice Id</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Mobile number</th>
                            <th>Address</th>
                            <th>Paid Amount</th>
                            <th>Status</th>   
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Loop through branches -->
                        <?php
                        $id = 1;
                        foreach ($users as $users):
                        ?>
                            <tr>
                                <td><?= $id;?></td>
                                <td><?= $users['invoice_number'] ?></td>
                                <td><?= $users['invoice_date'] ?></td>
                                <td><?= $users['name'] ?></td>
                                <td><?= $users['mobile_no'] ?></td>
                                <td><?= $users['address'] ?></td>
                                <td><?= $users['paid_amount'] ?></td>
                                <td><?= $users['status'] ?></td>
                                <td>
                                    <a href="<?= base_url('edit_pro_invoice/'.$users['id']) ?>"><i class="fas fa-edit" style="color:green;"></i></a>|
                                    <a href="<?= base_url('delete_pro_invoice/'.$users['id']) ?>" ><i class="fa fa-trash" style="color:red;"></i></a>
                                    <a href="#" class="view-details" data-invoice-id="<?= $users['id'] ?>"><i class="fas fa-eye" style="color:blue;"></i></a>
                                     <a href="<?= site_url('generatePdf/' . $users['id']) ?>" target="_blank">
        <i class="fas fa-print" style="color:purple;"></i>
    </a>
                                </td>
                            </tr>
                            <?php $id++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Product Details -->
<div class="modal fade" id="invoiceDetailsModal" tabindex="-1" role="dialog" aria-labelledby="invoiceDetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="invoiceDetailsModalLabel">Invoice Details</h5>
                <button type="button" class="close" aria-label="Close" id="closeModal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Content will be dynamically loaded here via AJAX -->
                <table class="table table-bordered" id="productTable">
                    <!-- Table for displaying product details -->
                    <thead>
                        <tr>
                            <th>S no</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
               <div class="text-right">
    <p id="grandTotal" class="mr-3"></p>
</div>
            </div>
        </div>
    </div>
</div>

<!-- Script to handle the click on the eye icon -->
<!-- Script to handle the click on the eye icon -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

<script>
    $(document).ready(function () {
        $('.view-details').on('click', function (e) {
            e.preventDefault();

            var invoiceId = $(this).data('invoice-id');

            $.ajax({
                url: '<?= base_url('fetch_invoice_details') ?>',
                type: 'POST',
                data: { invoiceId: invoiceId },
                dataType: 'json',
                success: function (response) {
                    if (response && response.invoice_products && response.invoice_products.length > 0) {
                        // Assuming you have a function to update the modal content
                        updateInvoiceDetailsModal(response);
                    } else {
                        // Handle the case where invoice_products array is empty or not present
                        console.error('Error fetching invoice details or empty invoice_products array.');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error or show a message if needed
                    console.error('Error fetching invoice details.', textStatus, errorThrown);
                }
            });
        });

        // Function to update the modal content
        function updateInvoiceDetailsModal(data) {
            console.log('Updating modal content:', data);

            $('#productTable tbody').empty();

            // Update the invoice product details table with the fetched data
            var grandTotal = 0;
            $.each(data.invoice_products, function (index, product) {
                grandTotal += parseFloat(product.total);
                var row = '<tr>' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td>' + product.product_name + '</td>' +
                    '<td>' + product.quantity + '</td>' +
                    '<td>' + product.price + '</td>' +
                    '</tr>';
                $('#productTable tbody').append(row);
            });

            // Update the grand total
            $('#grandTotal').text('Grand Total: ' + grandTotal.toFixed(2));

            // Show the modal
            $('#invoiceDetailsModal').modal('show');
        }

        // Manually close the modal when the close button is clicked
        $('#closeModal').on('click', function () {
            $('#invoiceDetailsModal').modal('hide');
        });
    });
</script>


<?=$this->endsection();?>
