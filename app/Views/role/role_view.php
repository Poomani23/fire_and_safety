
<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>





<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tables</h1>

<!-- DataTales Example -->
 <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Manage role</h6>
        <div class="text-right">
       
      
    <a href="<?php echo site_url('/add_role') ?>" class="btn btn-primary btn-icon-split">   
<span class="icon text-white-900">
<i class="fa fa-plus"></i>
</span>
<span class="text">ADD</span>
</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>First name</th>
                                <th>last name</th>
                                <th>User name</th>
                                <th>Emp_id</th>
                                <th> Role</th>
                                <th>Email</th>   
                                <th>Phone</th>
                                <th>Branch</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
            <!-- Loop through branches -->
             <?php
                $id = 1;
                foreach ($users as $users):
             ?>
                <tr>
                    <td><?= $id;?></td>
                    <td><?= $users['fname'] ?></td>
                    <td><?= $users['lname'] ?></td>
                    <td><?= $users['username'] ?></td>
                    <td><?= $users['emp_id'] ?></td>
                    <td><?= $users['role'] ?></td>
                    <td><?= $users['email'] ?></td>
                    <td><?= $users['phone'] ?></td> 
                    <td><?= $users['branch'] ?></td>
                    <td><?= $users['status'] ?></td>
                    <td>
                        <a href="<?= base_url('edit_role/'.$users['id']) ?>"><i class="fas fa-edit" style="color:green;"></i></a>|
                        <a href="<?= base_url('delete/'.$users['id']) ?>" ><i class="fa fa-trash" style="color:red;"></i></a>

                    </td>
                </tr>
                <?php $id++; ?>
            <?php endforeach; ?>
        </tbody>

                        <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>First name</th>
                                <th>last name</th>
                                <th>User name</th>
                                <th>Emp_id</th>
                                <th> Role</th>
                                <th>Email</th>   
                                <th>Phone</th>
                                <th>Branch</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
               
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->


<!-- End of Main Content -->



<?=$this->endsection();?>
