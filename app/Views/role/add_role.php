
<?=$this->extend('admin/admin')?>
<?= $this->section('content'); ?>
<style>
    .error {
    color: red !important;
    font-size: 15px !important;
    padding-left: 8px !important;
}
</style>

   <!-- Begin Page Content -->
   <div class="container-fluid">

<!-- Page Heading -->
<div class="text-right">
       
     <a href="<?php echo site_url('/manage_role') ?>" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-900">
            <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text">Back</span>
    </a>
        </div>
<div class="col-lg-8" style="margin: auto;">
    <div class="p-5">
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Employee-details</h1>
        </div>
     <form method="post" class="user" id="add_create" name="add_create" action="<?= site_url('/submit-form') ?>">
            <div class="form-group row">
                <div class="col-sm-6 ">
                     <input type="text" class="form-control" id="fname"
                        placeholder="First name" name="fname" >
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="lname"
                        placeholder="Last name" name="lname" >
                </div> 
            </div>
            <div class="form-group row">    
                <div class="col-sm-6">
                <input type="text" class="form-control" id="user_name"
                        placeholder="User name" name="username" >

                </div>
                <div class="col-sm-6">
                <input type="email" class="form-control" id="email"
                        placeholder="Email" name="email" >
                </div>
                
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                <select name="role" class="form-control" id="role" >
                            <option value="">Select role</option>
                            <option value="manager">Manager</option>
                            <option value="billingofficer">Billing officer</option>
                        </select>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="phone"  name="phone"  id="phone" >
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                <select class="form-control" id="branch" name="branch">
        <option value="">Select Branch</option>
        <?php foreach ($branches as $branch): ?>
            <option value="<?= $branch['branch_location']; ?>"><?= $branch['branch_location']; ?></option>
        <?php endforeach; ?>
    </select>                </div>
                <div class="col-sm-6 mb-3 mb-sm-0">
                 
                          <select name="status" class="form-control" id="status" >
                            <option value="">Select status</option>
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                   <input type="password" class="form-control" id="password"
                        placeholder="Password" name="password" >         
                </div>
                 <div class="col-sm-6">
                   <input type="password" class="form-control" id="confirm_password"
                        placeholder="Confirm Password" name="confirm_password" >         
                </div>
            </div>
             
        <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-block">
          
        </form>
    </div>
</div>

</div>
<!-- /.container-fluid -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
   if ($("#add_create").length > 0) {
  $("#add_create").validate({
    rules: {
      fname: {
        required: true,
      },
      lname: {
        required: true,
      },
      username: {
        required: true,
      },
      role: {
        required: true,
      },
      email: {
        required: true,
        maxlength: 60,
        email: true,
      },
      phone: {
        required: true,
        number: true,
        minlength: 10,
        maxlength: 10,
      },
      branch: {
        required: true,
      },
      password: {
        required: true,
        equalTo: "#confirm_password", // Fix: Reference the correct ID for confirmation
      },
      confirm_password: {
        required: true,
      },
      status: {
        required: true,
      },
    }, // <-- Add a comma here to separate rules and messages

    messages: {
      fname: {
        required: "First Name is required.",
      },
      lname: {
        required: "Last Name is required.",
      },
      username: {
        required: "User Name is required.",
      },
      role: {
        required: "Please select Your role.",
      },
      phone: {
        required: "Please enter your phone number",
        number: "Please enter a valid phone number",
        minlength: "Please enter at least 10 digits",
        maxlength: "Please do not enter more than 10 digits",
      },
      email: {
        required: "Email is required.",
        email: "It does not seem to be a valid email.",
        maxlength: "The email should be or equal to 60 chars.",
      },
      address: {
        required: "branch  is required.",
      },
      password: {
        required: "Please enter your Password",
      },
      confirm_password: {
        required: "Please enter your confirm password",
      },
      status: {
        required: "Please select Your status.",
      },
    },
  });
}

  </script>


<?=$this->endsection();?>
