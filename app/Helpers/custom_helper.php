
<?php
if (!function_exists('convertToWords')) {
    function convertToWords($amount) {
    $units = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    $teens = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    $tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $thousands = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];

    $numInWords = '';

    // Split the amount into groups of three digits
    $groups = array_reverse(str_split(str_pad($amount, ceil(strlen($amount) / 3) * 3, '0', STR_PAD_LEFT), 3));

    foreach ($groups as $key => $group) {
        $group = (int) $group;

        if ($group > 0) {
            $hundreds = floor($group / 100);
            $tensAndUnits = $group % 100;

            if ($hundreds > 0) {
                $numInWords .= $units[$hundreds] . ' Hundred ';
            }

            if ($tensAndUnits > 0) {
                if ($tensAndUnits < 10) {
                    $numInWords .= $units[$tensAndUnits];
                } elseif ($tensAndUnits < 20) {
                    $numInWords .= $teens[$tensAndUnits - 10];
                } else {
                    $numInWords .= $tens[floor($tensAndUnits / 10)] . ' ' . $units[$tensAndUnits % 10];
                }
            }

            $numInWords .= ' ' . $thousands[$key] . ' ';
        }
    }

    // Remove extra spaces and return the result
    return trim($numInWords);
}
}
