<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\BranchModel;
use App\Models\CategoryModel;
use App\Models\ServiceModel;
use App\Models\ProductModel;
use App\Models\InvoiceModel;
use App\Models\InvoiceProductModel;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceItemModel;
use App\Models\RoleModel;

class BillingOfficerController extends BaseController
{
    public function billingofficer_dashboard()
    {  
        // Retrieve the username from the session
        $username = session()->get('loggedInUser')['username'];
        $role = session()->get('loggedInUser')['role'];
      
                
        $branch = session()->get('loggedInUser')['branch'];
       
       
        // Pass the username to the view
        return view('layout/billingofficer_dashboard', ['username' => $username], ['role' => $role], ['branch' => $branch]);
    }
    

}
