<?php

namespace App\Controllers;
use App\Models\CategoryModel;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;


class CategoryController extends BaseController
{
    public function index()
    {

        $isAdmin = true; 

    $data = [
        'isAdmin' => $isAdmin,
    ];
        return view('category/manage_category',$data);
    }

    public function add_category()
    {

        $isAdmin = true; 

    $data = [
        'isAdmin' => $isAdmin,
    ];
        return view('category/add_category',$data);
    }

    public function save()
{
    $categoryModel = new CategoryModel(); 

    $data = [
        'category_name' => $this->request->getPost('category_name'),
        'status' => $this->request->getPost('status'),
        'isAdmin' => true,
    ];

    $categoryModel->insert($data);

    return redirect()->to(base_url('manage_category'));


}



   public function manage()
{
    $categoryModel = new CategoryModel();

    // Retrieve category with 'active' status
    $data['categories'] = $categoryModel->where('status', 'available')->findAll();

    return view('category/manage_category', $data);
}

    public function edit($id)
    {
        $categoryModel = new CategoryModel();
        $data['category'] = $categoryModel->find($id); // Fetch category details by ID

        // Load the edit view with category details
        return view('category/edit_category', $data);
    }
     public function update($id)
    {
        $categoryModel = new CategoryModel();
        $data = [
            'category_name' => $this->request->getPost('category_name'),
            'status' => $this->request->getPost('status')
        ];

        // Update the branch details in the database
        $categoryModel->update($id, $data);

        // Redirect to the manage branches page after updating
        return redirect()->to(base_url('manage_category'));
    }

    public function delete($id)
{
    $categoryModel = new CategoryModel();
    
   
    $data = ['status' => 'unavailable'];
    $categoryModel->update($id, $data);
    return redirect()->to(base_url('manage_category'));
}


}
