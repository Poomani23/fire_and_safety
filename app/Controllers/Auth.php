<?php


namespace App\Controllers;


use App\Controllers\BaseController;
use App\Controllers\CommonSettingController;
use App\Libraries\Hash;
use App\Models\UserModel;
use App\Models\CommonSettingModel;
use App\Models\RoleModel;


class Auth extends BaseController
{


    // Enabling features
    public function __construct()
    {
        helper(['url', 'form']);
    }



    /**
      * Responsible for login page view.
    */
    public function index()
    {       
         $commonSettingModel = new CommonSettingModel();

        $data['loginImgURL'] = base_url('public/uploads/' . $commonSettingModel->getLoginImageURL());

        $error = session()->getFlashdata('error');
        return view('auth/login', ['error' => $error, 'loginImgURL' => $data['loginImgURL']]);    }


    /**
      *Responsible for register page view.
    */  
    public function register()
    {       
         $commonSettingModel = new CommonSettingModel();
        $data['regImgURL'] = base_url('public/uploads/' . $commonSettingModel->getRegisterImageURL());

        return view('auth/register' , ['regImgURL' => $data['regImgURL']]);
    }


    /**
     * Save new user to database.
     */


     public function registerUser()
     {
         // Validate user input.


        //  $validated = $this->validate([
        //     'name'=> 'required',
        //     'email' => 'required|valid_email',
        //     'password' => 'required|min_length[5]|max_length[20]',
        //     'passwordConf'=> 'required|min_length[5]|max_length[20]|matches[password]'
        //  ]);


        $validated = $this->validate([
            'firstname' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Your first name is required', 
                ]
            ],
             'lastname' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Your last name is required', 
                ]
            ],
            'email'=> [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Your email is required', 
                    'valid_email' => 'Email is already used.',
                ]
            ],
            'password'=> [
                'rules' => 'required|min_length[5]|max_length[20]',
                'errors' => [
                    'required' => 'Your password is required', 
                    'min_length' => 'Password must be 5 charectars long',
                    'max_length' => 'Password cannot be longer than 20 charectars'
                ]
            ],
            'confirm_password'=> [
                'rules' => 'required|min_length[5]|max_length[20]|matches[password]',
                'errors' => [
                    'required' => 'Your confirm password is required', 
                    'min_length' => 'Password must be 5 charectars long',
                    'max_length' => 'Password cannot be longer than 20 charectars',
                    'matches' => 'Confirm password must match the password',
                ]
            ],
        ]);


         if(!$validated)
         {
             return view('auth/register', ['validation' => $this->validator]);
         }


         // Here we save the user.


         $firstname = $this->request->getPost('firstname');
           $lastname = $this->request->getPost('lastname');
           $username = $firstname . ' ' . $lastname;
         $email = $this->request->getPost('email');
         $password = $this->request->getPost('password');
         $confirm_password = $this->request->getPost('confirm_password');
          $status = $this->request->getPost('status');



         $data = [
            'firstname' => $firstname,
            'lastname' => $lastname,
              'username' => $username,
            'email' => $email,
            'password' => Hash::encrypt($password),
             'confirm_password' => Hash::encrypt($confirm_password),
            'status' => $status
         ];


         // Storing data


         $userModel = new \App\Models\UserModel();
         $query = $userModel->insert($data);


        if(!$query)
        {
            return redirect()->back()->with('fail', 'Saving user failed');
        }
        else
        {
             session()->setFlashdata('success', 'User Register successful!');
            return redirect()->to('login');
        }
     }


     /**
      * User login method.
      */
      public function loginUser()
{
    // Validating user input.
    $validated = $this->validate([
        'username' => [
            'rules' => 'required',
            'errors' => [
                'required' => 'Your username is required'
            ]
        ],
        'password' => [
            'rules' => 'required|min_length[5]|max_length[20]',
            'errors' => [
                'required' => 'Your password is required', 
                'min_length' => 'Password must be 5 characters long',
                'max_length' => 'Password cannot be longer than 20 characters'
            ]
        ],
    ]);

    if (!$validated) {
        return view('auth/login', ['validation' => $this->validator]);
    } else {
        // Checking user details for admin login.
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        
        if ($username === 'admin' && $password === 'admin@123') {
            // Set admin details in session upon successful login
            $adminDetails = [
                'username' => 'admin',
                'isAdmin' => true
            ];

            session()->set('loggedInUser', $adminDetails);

            // Redirect to admin dashboard
            return redirect()->to('admin_dashboard');
        }

        // If not admin, proceed to check regular user login
        $roleModel = new RoleModel();
        $userInfo = $roleModel->where('username', $username)->first();

        if ($userInfo !== null) {
            // If the user exists, verify their password
            if (Hash::check($password, $userInfo['password'])) {
                $userRole = $roleModel->where('username', $username)->first()['role'];
                
                // Check the role of the user
                switch ($userRole) {
                    case 'manager':
                        // Retrieve the 'branch' information for the manager
                        $managerBranch = $roleModel->where('username', $username)->first()['branch'];
                        
                        // Set manager details in session upon successful login
                        $managerDetails = [
                            'username' => $userInfo['username'],
                            'role' => $userRole,
                            'branch' => $managerBranch,
                            'isLoggedIn' => true
                        ];
                        
                        session()->set('loggedInUser', $managerDetails);
                        
                        // Redirect to manager dashboard
                        return redirect()->to('manager/manager_dashboard');
                        break;
                    case 'billingofficer':
                        $OfficerBranch = $roleModel->where('username', $username)->first()['branch'];

                        // Set billing officer details in session upon successful login
                        $billingOfficerDetails = [
                            'username' => $userInfo['username'],
                            'role' => $userRole,
                            'branch' => $OfficerBranch,
                            'isLoggedIn' => true
                        ];

                        session()->set('loggedInUser', $billingOfficerDetails);

                        // Redirect to billing officer dashboard
                        return redirect()->to('billingofficer/billingofficer_dashboard');
                        break;
                    default:
                        // Redirect to default user dashboard
                        session()->setFlashdata('fail', 'Incorrect username or password provided');
                        return redirect()->to('login');
                        break;
                }
                
            } else {
                // Incorrect password
                session()->setFlashdata('fail', 'Incorrect password provided');
                return redirect()->to('login');
            }
        } else {
            // User record not found for the provided username
            session()->setFlashdata('fail', 'Incorrect username or password');
            return redirect()->to('login');
        }
    }
}

      


/**
 * Log out the user.
 */
public function logout()
{

    
    if (session()->has('loggedInUser')) {
        session()->remove('loggedInUser');
    }

    return redirect()->to('/login?access=loggedout')->with('fail', 'You are logged out');
}

}
