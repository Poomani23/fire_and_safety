<?php

namespace App\Controllers\Manager;
use CodeIgniter\Controller;
use App\Models\InvoiceModel;
use App\Models\RoleModel;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;
use App\Models\BranchModel;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductPricesModel;
use App\Models\ProductModel;
use App\Models\InvoiceProductModel;
use App\Models\ManagersettingModel;
use Dompdf\Dompdf;
use Dompdf\Options;

class InvoiceController extends BaseController
{     
    use ResponseTrait;
    public function index()
    {
        $branchModel = new BranchModel();

        // Get the branch information of the logged-in user
        $branch = session()->get('loggedInUser')['branch'];
      

        // Fetch all invoice data from the database
        $model = new InvoiceModel();
        $branch = session()->get('loggedInUser')['branch'];

        // Fetch invoices for the logged-in user's branch where status is 'paid'
        $data['invoices'] = $model->where('branch', $branch)
                                  ->findAll();
        
        // Pass the branch details to the view

        // Load the view for managing invoices
        return view('manager/invoice/manage_invoice', $data);
    }







 
    







// Helper method to save invoice data

// Helper method to fetch invoice details
protected function fetchInvoiceDetails($invoiceId)
{
    $invoiceModel = new InvoiceModel();
    $invoice = $invoiceModel->find($invoiceId);

    $invoiceProductModel = new InvoiceProductModel();
    $invoiceProducts = $invoiceProductModel->where('invoice_id', $invoiceId)->findAll();

    $productDetails = [];
    foreach ($invoiceProducts as $invoiceProduct) {
        $productModel = new ProductModel();
        $product = $productModel->find($invoiceProduct['product_id']);

        $productDetails[] = [
            'product_name' => $product['product_name'],
            'quantity'     => $invoiceProduct['quantity'],
            'price'        => $invoiceProduct['price'],
            'total'        => $invoiceProduct['total'],
        ];
    }

    return ['invoice' => $invoice, 'products' => $productDetails];
}




// Assuming this method is in your InvoiceController.php

// Example in InvoiceController.php
// InvoiceController.php

public function getProductNamesByBranch()
{
    try {
        // Get the branch_location from the POST data
        $branch_location = $this->request->getPost('branch_location');

        // Load the required models
        $productPricesModel = new \App\Models\ProductPricesModel();
        $productModel = new \App\Models\ProductModel();
        $branchModel = new \App\Models\BranchModel();

        // Validate $branch_location
        if (empty($branch_location)) {
            // Return an error response if $branch_location is empty
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location is required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Build the query to fetch available product names
            $query = $productPricesModel->select('products.product_name')
                ->distinct()
                ->join('products', 'products.id = product_prices.product_id')
                ->where('product_prices.branch_id', $branch_id)
                ->where('products.status', 'available');

            // Fetch results
            $product_names = $query->get()->getResultArray();

            // Return product names as JSON
            return $this->response->setJSON(['product_names' => $product_names]);
        } else {
            // Return an error response if the branch is not found
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        // Return an error response if an exception occurs
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function getProductPrice()
{
    try {
        $branch_location = $this->request->getPost('branch_location');
        $product_name = $this->request->getPost('product_name');

        // Load the required models
        $productPricesModel = new \App\Models\ProductPricesModel();
        $branchModel = new \App\Models\BranchModel();
        $productModel = new \App\Models\ProductModel();

        // Validate branch_location and product_name
        if (empty($branch_location) || empty($product_name)) {
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location and product name are required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Fetch product_id from products table using product_name
            $product = $productModel->where('product_name', $product_name)->first();

            if ($product) {
                $product_id = $product['id'];

                // Build the query to fetch product price
                $query = $productPricesModel
                    ->select('price')
                    ->where('branch_id', $branch_id)
                    ->where('product_id', $product_id)
                    ->limit(1);

                // Fetch the result
                $result = $query->get()->getRow();

                if ($result) {
                    $price = $result->price;
                    return $this->response->setJSON(['price' => $price]);
                } else {
                    return $this->response->setStatusCode(404)->setJSON(['error' => 'Product price not found']);
                }
            } else {
                return $this->response->setStatusCode(404)->setJSON(['error' => 'Product not found']);
            }
        } else {
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function fetch_invoice_details()
{
    $request = \Config\Services::request();
    $invoiceId = $request->getPost('invoiceId');

    $invoiceModel = new \App\Models\InvoiceModel();
    $invoiceDetails = $invoiceModel->getInvoiceWithProducts($invoiceId);


    return $this->response->setJSON($invoiceDetails);
}  
public function product_invoice_report()
    {
        return view('manager/invoice/invoice_report_form');
    }

    public function displayInvoiceReport()
{
    // Assuming you have a model named InvoiceModel
    $invoiceModel = new InvoiceModel();
    $branchModel = new BranchModel();

    // Get the selected date range (you need to get this from your form)
    $startDate = $this->request->getPost('start_date');
    $endDate = $this->request->getPost('end_date');

    // Get the branch information of the logged-in user
    $loggedInBranch = session()->get('loggedInUser')['branch'];

    // Fetch invoices between the selected date range with status "paid" for the logged-in user's branch
    $invoices = $invoiceModel
        ->where('status', 'paid')
        ->where('invoice_date >=', $startDate)
        ->where('invoice_date <=', $endDate)
        ->where('branch', $loggedInBranch)
        ->findAll();

    // Calculate the overall total of paid_amount
    $overallTotal = array_sum(array_column($invoices, 'grand_total'));

    // Pass the data to the view, including start and end dates
    $data = [
        'invoices' => $invoices,
        'overallTotal' => $overallTotal,
        'startDate' => $startDate,
        'endDate' => $endDate,
    ];

    return view('manager/invoice/invoice_product_report', $data);
}

    
    

}