<?php

namespace App\Controllers\Manager;
use CodeIgniter\Controller;
use App\Models\InvoiceModel;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;
use App\Models\BranchModel;
use App\Models\ProductPricesModel;
use App\Models\ProductModel;
use App\Models\InvoiceProductModel;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceModel;


use App\Models\ServiceItemModel;


use Dompdf\Dompdf;
use Dompdf\Options;
use App\Models\ServicePricesModel;
use App\Models\CommonSettingModel;


class ServiceInvoiceController extends BaseController
{     
 
    public function index()
{
    $branchModel = new BranchModel();

    // Get the branch information of the logged-in user
    $branch = session()->get('loggedInUser')['branch'];

    $model = new ServiceInvoiceModel();
    $branch = session()->get('loggedInUser')['branch'];
  
  

   
    $data['sr_invoices'] = $model->where('branch_location', $branch)->findAll();


    

    // Load the view for managing invoices
    return view('manager/serviceInvoice/manage_service_invoice', $data);
   
}








public function getServiceItemsByBranch()
{
    try {
        // Get the branch_location from the POST data
        $branch_location = $this->request->getPost('branch_location');

        // Load the required models
        $servicePricesModel = new \App\Models\servicePricesModel();
        $serviceModel = new \App\Models\serviceModel();
        $branchModel = new \App\Models\BranchModel();

        // Validate $branch_location
        if (empty($branch_location)) {
            // Return an error response if $branch_location is empty
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location is required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Build the query to fetch available product names
            $query = $servicePricesModel->select('service.sr_item')
                ->distinct()
                ->join('service', 'service.id = service_prices.sr_id')
                ->where('service_prices.branch_id', $branch_id)
                ->where('service.status', 'available');

            // Fetch results
            $service_items = $query->get()->getResultArray();

            // Return service item as JSON
            return $this->response->setJSON(['service_items' => $service_items]);
        } else {
            // Return an error response if the branch is not found
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        // Return an error response if an exception occurs
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function getServicePrice()
{
    try {
        $branch_location = $this->request->getPost('branch_location');
        $sr_item = $this->request->getPost('sr_item');

        // Load the required models
        $ServicePricesModel = new \App\Models\ServicePricesModel();
        $branchModel = new \App\Models\BranchModel();
        $ServiceModel = new \App\Models\ServiceModel();

        // Validate branch_location and sr_item
        if (empty($branch_location) || empty($sr_item)) {
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location and service item are required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Fetch product_id from products table using sr_item
            $service = $ServiceModel->where('sr_item', $sr_item)->first();

            if ($service) {
                $sr_id = $service['id'];

                // Build the query to fetch service price
                $query = $ServicePricesModel
                    ->select('price')
                    ->where('branch_id', $branch_id)
                    ->where('sr_id', $sr_id)
                    ->limit(1);

                // Fetch the result
                $result = $query->get()->getRow();

                if ($result) {
                    $price = $result->price;
                    return $this->response->setJSON(['price' => $price]);
                } else {
                    return $this->response->setStatusCode(404)->setJSON(['error' => 'Product price not found']);
                }
            } else {
                return $this->response->setStatusCode(404)->setJSON(['error' => 'Product not found']);
            }
        } else {
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
} 
        










public function fetch_sr_invoice_details()
{
    $request = \Config\Services::request();
    $srinvoiceId = $request->getPost('srinvoiceId');

    $serviceinvoiceModel = new \App\Models\ServiceInvoiceModel();
    $srinvoiceDetails = $serviceinvoiceModel->getInvoiceWithService($srinvoiceId);


    return $this->response->setJSON($srinvoiceDetails);
}


public function service_invoice_report()
    {
        return view('manager/serviceInvoice/service_report_form');
    }

    public function displaySrInvoiceReport()
    {
        $ServiceInvoiceModel = new ServiceInvoiceModel();
    
    $branchModel = new BranchModel();

    // Get the selected date range (you need to get this from your form)
    $startDate = $this->request->getPost('start_date');
    $endDate = $this->request->getPost('end_date');

    // Get the branch information of the logged-in user
     $loggedInBranch = session()->get('loggedInUser')['branch'];
     
        
    
        // Fetch invoices between the selected date range with status "paid"
        $invoices = $ServiceInvoiceModel
            ->where('status', 'paid')
            ->where('service_date >=', $startDate)
            ->where('service_date <=', $endDate)
            ->where('branch_location', $loggedInBranch)
            ->findAll();
    
   
        // Calculate the overall total of paid_amount
        $overallTotal = array_sum(array_column($invoices, 'grand_total'));
    
        // Pass the data to the view, including start and end dates
        $data = [
            'invoices' => $invoices,
            'overallTotal' => $overallTotal,
            'startDate' => $startDate,  // Add this line
            'endDate' => $endDate,      // Add this line
        ];
    
        return view('manager/serviceinvoice/invoice_service_report', $data);
    }

}