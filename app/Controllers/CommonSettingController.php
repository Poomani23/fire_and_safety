<?php

namespace App\Controllers;

use App\Models\CommonSettingModel;
use CodeIgniter\Controller;

class CommonSettingController extends BaseController
{
    protected $commonSettingModel;

    public function __construct()
    {
        $this->commonSettingModel = new CommonSettingModel();
    }

    public function index()
    {
        $data['row'] = $this->commonSettingModel->first();

        if (!$data['row']) {
            $data['row'] = [
                'shop_name' => '',
                'profile_img' => '',
                'login_img' => '',
                'reg_img' => '',
                'invoice_prefix' => '',
            ];
        }

        return view('layout/common_setting', $data);
    }
    public function update()
{
    helper(['form', 'url']);

    $shop_name = $this->request->getPost('shop_name');
    $invoice_prefix = $this->request->getPost('invoice_prefix');

    // Process file uploads and validations
    $profile_image = $this->request->getFile('profile_img');
    $login_image = $this->request->getFile('login_img');
    $reg_image = $this->request->getFile('reg_img');

    // Retrieve the existing data
    $existingData = $this->commonSettingModel->first();

    // Check if the record exists
    if ($existingData) {
        // Update the data in the database
        $data = [
            'shop_name' => $shop_name,
            'invoice_prefix' => $invoice_prefix,
        ];

        // Handle profile image
        if ($profile_image && $profile_image->isValid() && !$profile_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $profile_image->getRandomName();
            $profile_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['profile_img'] = $newImageName;
        }

        // Handle login image
        if ($login_image && $login_image->isValid() && !$login_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $login_image->getRandomName();
            $login_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['login_img'] = $newImageName;
        }

        // Handle register image
        if ($reg_image && $reg_image->isValid() && !$reg_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $reg_image->getRandomName();
            $reg_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['reg_img'] = $newImageName;
        }

        // Update the existing data in the database
        $this->commonSettingModel->update($existingData['id'], $data);

        return redirect()->to(site_url('common_setting'))->with('success', 'Settings updated successfully');
    } else {
        // Insert a new record into the database
        $data = [
            'shop_name' => $shop_name,
            'invoice_prefix' => $invoice_prefix,
        ];

        // Handle profile image
        if ($profile_image && $profile_image->isValid() && !$profile_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $profile_image->getRandomName();
            $profile_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['profile_img'] = $newImageName;
        }

        // Handle login image
        if ($login_image && $login_image->isValid() && !$login_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $login_image->getRandomName();
            $login_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['login_img'] = $newImageName;
        }

        // Handle register image
        if ($reg_image && $reg_image->isValid() && !$reg_image->hasMoved()) {
            // Validate and move the uploaded file to a designated directory
            $uploadPath = 'public/uploads';
            $newImageName = $reg_image->getRandomName();
            $reg_image->move($uploadPath, $newImageName);

            // Set the new image name in the data array
            $data['reg_img'] = $newImageName;
        }

        // Insert a new record into the database
        $this->commonSettingModel->insert($data);

        return redirect()->to(site_url('common_setting'))->with('success', 'Settings inserted successfully');
    }

    return redirect()->to(site_url('common_setting'))->with('error', 'Record not found');
}

            
    
}