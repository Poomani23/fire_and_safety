<?php

namespace App\Controllers\BillingOfficer;
use CodeIgniter\Controller;
use App\Models\InvoiceModel;
use App\Models\RoleModel;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;
use App\Models\BranchModel;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductPricesModel;
use App\Models\ProductModel;
use App\Models\InvoiceProductModel;
use App\Models\commonsettingModel;
use Dompdf\Dompdf;
use Dompdf\Options;

class InvoiceController extends BaseController
{     
    use ResponseTrait;
    public function index()
    {
        $branchModel = new BranchModel();

        // Get the branch information of the logged-in user
        $branch = session()->get('loggedInUser')['branch'];
      

        // Fetch all invoice data from the database
        $model = new InvoiceModel();
        $branch = session()->get('loggedInUser')['branch'];

        // Fetch invoices for the logged-in user's branch where status is 'paid'
        $data['invoices'] = $model->where('branch', $branch)
                                  ->findAll();
        
        // Pass the branch details to the view

        // Load the view for managing invoices
        return view('billingofficer/invoice/manage_invoice', $data);
    }


    public function add_invoice()
    {
        $branchModel = new BranchModel();
        $branches = $branchModel->where('status', 'active')->findAll();
        $data['branches'] = $branches;

        $commonSettingModel = new commonSettingModel();
        $invoicePrefixSetting = $commonSettingModel->getFirstSetting();

        if (!$invoicePrefixSetting) {
            $prefix = 'invoicepro';
        } else {
            $prefix = $invoicePrefixSetting['invoice_prefix'];
        }

        $invoiceModel = new InvoiceModel();
        $lastInvoice = $invoiceModel->orderBy('invoice_number', 'DESC')->first();

        if ($lastInvoice) {
            $lastId = intval(preg_replace('/[^0-9]/', '', $lastInvoice['invoice_number']));
            $nextId = $lastId + 1;
    
            $invoiceNumber = $prefix . str_pad($nextId, 3, '0', STR_PAD_LEFT);

            while ($invoiceModel->where('invoice_number', $invoiceNumber)->countAllResults() > 0) {
                $nextId++;
                $invoiceNumber = $prefix . str_pad($nextId, 3, '0', STR_PAD_LEFT);
            }
        } else {
            $invoiceNumber = $prefix . '001';
        }

        $data['invoiceNumber'] = $invoiceNumber;
        
        

        return view('billingofficer/invoice/add_invoice', $data);
    }




    public function save_invoice()
    {
        if ($this->request->getMethod() === 'post') {
            $invoiceNumber = $this->request->getPost('invoice_no');
    
            $invoiceData = [
                'invoice_number' => $invoiceNumber,
                'invoice_date'   => $this->request->getPost('invoice_date'),
                'mobile_no'      => $this->request->getPost('cnumber'),
                'name'           => $this->request->getPost('cr_name'),
                'address'        => $this->request->getPost('caddress'),
                'branch'         => $this->request->getPost('branch'),
                'payment_type'   => $this->request->getPost('payment_type'),
                'paid_amount'    => $this->request->getPost('paid_amount'),
                'grand_total'    => $this->request->getPost('grand_total'),
                'status'         => $this->request->getPost('status'),
            ];
    
            $db = db_connect();
            $builder = $db->table('invoices');
            $builder->insert($invoiceData);
    
            if (!$builder) {
                die("Failed to insert data into invoices table.");
            }
    
            // Use the builder directly to get the inserted ID
            $invoiceId = $db->insertID();
    
            if (!$invoiceId) {
                die("Failed to retrieve the inserted ID from invoices table.");
            }
    
            $productNames = $this->request->getPost('pname');
            $prices = $this->request->getPost('price');
            $quantities = $this->request->getPost('qty');
    
            foreach ($productNames as $key => $productName) {
                $productModel = new ProductModel();
    
                $product = $productModel->where('product_name', $productName)->first();
    
                if (!$product) {
                    $productModel->insert(['product_name' => $productName]);
                    $product = $productModel->where('product_name', $productName)->first();
                }
    
                $invoiceProductData = [
                    'invoice_id' => $invoiceId,
                    'product_id' => $product['id'],
                    'quantity'   => $quantities[$key],
                    'price'      => $prices[$key],
                    'total'      => $quantities[$key] * $prices[$key],
                ];
    
                $invoiceProductModel = new InvoiceProductModel();
                $invoiceProductModel->insert($invoiceProductData);
            }
    
            // Check if the request is from the print button
            if ($this->request->getPost('print')) {
                $invoiceNumber = $this->request->getPost('invoice_no');
    
                $invoiceData = [
                    'invoice_number' => $invoiceNumber,
                    'invoice_date'   => $this->request->getPost('invoice_date'),
                    'mobile_no'      => $this->request->getPost('cnumber'),
                    'name'           => $this->request->getPost('cr_name'),
                    'address'        => $this->request->getPost('caddress'),
                    'branch'         => $this->request->getPost('branch'),
                    'payment_type'   => $this->request->getPost('payment_type'),
                    'paid_amount'    => $this->request->getPost('paid_amount'),
                    'grand_total'    => $this->request->getPost('grand_total'),
                    'status'         => $this->request->getPost('status'),
                ];
                
                $db = db_connect();
                $builder = $db->table('invoices');
                $builder->insert($invoiceData);
        
                if (!$builder) {
                    die("Failed to insert data into invoices table.");
                }
        
                // Use the builder directly to get the inserted ID
                $invoiceId = $db->insertID();
        
                if (!$invoiceId) {
                    die("Failed to retrieve the inserted ID from invoices table.");
                }
        
                $productNames = $this->request->getPost('pname');
                $prices = $this->request->getPost('price');
                $quantities = $this->request->getPost('qty');
        
                foreach ($productNames as $key => $productName) {
                    $productModel = new ProductModel();
        
                    $product = $productModel->where('product_name', $productName)->first();
        
                    if (!$product) {
                        $productModel->insert(['product_name' => $productName]);
                        $product = $productModel->where('product_name', $productName)->first();
                    }
        
                    $invoiceProductData = [
                        'invoice_id' => $invoiceId,
                        'product_id' => $product['id'],
                        'quantity'   => $quantities[$key],
                        'price'      => $prices[$key],
                        'total'      => $quantities[$key] * $prices[$key],
                    ];

                    $invoiceProductModel = new InvoiceProductModel();
                    $invoiceProductModel->insert($invoiceProductData);
                }

                // Redirect to the print page with the newly inserted invoice ID
                return redirect()->to("/generatePdf/{$invoiceId}")->with('success', 'Invoice saved successfully.');
            } else {
                // Redirect to the manage page
                return redirect()->to("billingofficer/manage_invoice")->with('success', 'Invoice saved successfully.');
            }
        }
    }


    public function edit_invoice($id)
    {
        $branchModel = new BranchModel();
        $branches = $branchModel->where('status', 'active')->findAll();
        $data['branches'] = $branches;
    
        $invoiceModel = new InvoiceModel();
        $invoice = $invoiceModel->find($id);
    
        if (!$invoice) {
            return view('errors/html/error_404');
        }
    
        $invoiceProductModel = new InvoiceProductModel();
        $invoiceProducts = $invoiceProductModel->where('invoice_id', $id)->findAll();
    
        $productDetails = [];
        foreach ($invoiceProducts as $invoiceProduct) {
            $productModel = new ProductModel();
            $product = $productModel->find($invoiceProduct['product_id']);
    
            if ($product) {
                $productDetails[] = [
                    'product_name' => $product['product_name'],
                    'quantity' => $invoiceProduct['quantity'],
                    'price' => $invoiceProduct['price'],
                    'total' => $invoiceProduct['total'],
                ];
            }
        }
    
        $data['invoice'] = $invoice;
        $data['productDetails'] = $productDetails;
        $data['invoiceProducts'] = $invoiceProducts; // Add this line
    
        return view('billingofficer/invoice/edit_pro_invoice', $data);
    }
    
    public function update_invoice()
    {
        $id = $this->request->getPost('invoice_id');
    
        $invoiceData = [
            'invoice_date' => $this->request->getPost('invoice_date'),
            'mobile_no' => $this->request->getPost('cnumber'),
            'name' => $this->request->getPost('cr_name'),
            'address' => $this->request->getPost('caddress'),
            'payment_type' => $this->request->getPost('payment_type'),
            'paid_amount' => $this->request->getPost('paid_amount'),
            'status' => $this->request->getPost('status'),
        ];
    
        $db = db_connect();
        $builder = $db->table('invoices');
        $builder->where('id', $id)->update($invoiceData);
    
        if (!$builder) {
            die("Failed to update data in the invoices table.");
        }
    
        // Update existing invoice products
    
    
        return redirect()->to("billingofficer/manage_invoice")->with('success', 'Invoice updated successfully.');
    }
    public function delete_pro_invoice($id)
    {
        // Load the model
        $invoiceModel = new InvoiceModel();
    
    
        $invoice = $invoiceModel->find($id);
    
    
        $invoiceModel->set('status', 'not_paid')->where('id', $id)->update();
    
       
        return redirect()->to("billingofficer/manage_invoice")->with('success', 'Invoice canceled successfully.');
    }

 
    







// Helper method to save invoice data

// Helper method to fetch invoice details
protected function fetchInvoiceDetails($invoiceId)
{
    $invoiceModel = new InvoiceModel();
    $invoice = $invoiceModel->find($invoiceId);

    $invoiceProductModel = new InvoiceProductModel();
    $invoiceProducts = $invoiceProductModel->where('invoice_id', $invoiceId)->findAll();

    $productDetails = [];
    foreach ($invoiceProducts as $invoiceProduct) {
        $productModel = new ProductModel();
        $product = $productModel->find($invoiceProduct['product_id']);

        $productDetails[] = [
            'product_name' => $product['product_name'],
            'quantity'     => $invoiceProduct['quantity'],
            'price'        => $invoiceProduct['price'],
            'total'        => $invoiceProduct['total'],
        ];
    }

    return ['invoice' => $invoice, 'products' => $productDetails];
}




// Assuming this method is in your InvoiceController.php

// Example in InvoiceController.php
// InvoiceController.php

public function getProductNamesByBranch()
{
    try {
        // Get the branch_location from the POST data
        $branch_location = $this->request->getPost('branch_location');

        // Load the required models
        $productPricesModel = new \App\Models\ProductPricesModel();
        $productModel = new \App\Models\ProductModel();
        $branchModel = new \App\Models\BranchModel();

        // Validate $branch_location
        if (empty($branch_location)) {
            // Return an error response if $branch_location is empty
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location is required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Build the query to fetch available product names
            $query = $productPricesModel->select('products.product_name')
                ->distinct()
                ->join('products', 'products.id = product_prices.product_id')
                ->where('product_prices.branch_id', $branch_id)
                ->where('products.status', 'available');

            // Fetch results
            $product_names = $query->get()->getResultArray();

            // Return product names as JSON
            return $this->response->setJSON(['product_names' => $product_names]);
        } else {
            // Return an error response if the branch is not found
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        // Return an error response if an exception occurs
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function getProductPrice()
{
    try {
        $branch_location = $this->request->getPost('branch_location');
        $product_name = $this->request->getPost('product_name');

        // Load the required models
        $productPricesModel = new \App\Models\ProductPricesModel();
        $branchModel = new \App\Models\BranchModel();
        $productModel = new \App\Models\ProductModel();

        // Validate branch_location and product_name
        if (empty($branch_location) || empty($product_name)) {
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location and product name are required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Fetch product_id from products table using product_name
            $product = $productModel->where('product_name', $product_name)->first();

            if ($product) {
                $product_id = $product['id'];

                // Build the query to fetch product price
                $query = $productPricesModel
                    ->select('price')
                    ->where('branch_id', $branch_id)
                    ->where('product_id', $product_id)
                    ->limit(1);

                // Fetch the result
                $result = $query->get()->getRow();

                if ($result) {
                    $price = $result->price;
                    return $this->response->setJSON(['price' => $price]);
                } else {
                    return $this->response->setStatusCode(404)->setJSON(['error' => 'Product price not found']);
                }
            } else {
                return $this->response->setStatusCode(404)->setJSON(['error' => 'Product not found']);
            }
        } else {
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function fetch_invoice_details()
{
    $request = \Config\Services::request();
    $invoiceId = $request->getPost('invoiceId');

    $invoiceModel = new \App\Models\InvoiceModel();
    $invoiceDetails = $invoiceModel->getInvoiceWithProducts($invoiceId);


    return $this->response->setJSON($invoiceDetails);
}  


    
    
    

}