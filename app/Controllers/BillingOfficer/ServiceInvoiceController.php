<?php

namespace App\Controllers\BillingOfficer;
use CodeIgniter\Controller;
use App\Models\InvoiceModel;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;
use App\Models\BranchModel;
use App\Models\ProductPricesModel;
use App\Models\ProductModel;
use App\Models\InvoiceProductModel;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceModel;


use App\Models\ServiceItemModel;


use Dompdf\Dompdf;
use Dompdf\Options;
use App\Models\ServicePricesModel;
use App\Models\CommonSettingModel;


class ServiceInvoiceController extends BaseController
{     
 
    public function index()
{
    $branchModel = new BranchModel();

    // Get the branch information of the logged-in user
    $branch = session()->get('loggedInUser')['branch'];

    $model = new ServiceInvoiceModel();
    $branch = session()->get('loggedInUser')['branch'];
  
  

   
    $data['sr_invoices'] = $model->where('branch_location', $branch)
    ->where('status', 'paid')
    ->findAll();


    

    // Load the view for managing invoices
    return view('billingofficer/serviceInvoice/manage_service_invoice', $data);
   
}


public function add_service_invoice()
{
    // Fetch active branches for use in the view
    $branchModel = new BranchModel();
    $branches = $branchModel->where('status', 'active')->findAll();
    $data['branches'] = $branches;

    // Retrieve the common settings from the database
    $commonSettingModel = new CommonSettingModel();
    $invoicePrefixSetting = $commonSettingModel->getFirstSetting();

    if (!$invoicePrefixSetting) {
        $prefix = 'servicepro';
    } else {
        $prefix = $invoicePrefixSetting['invoice_prefix'];
    }

    // Retrieve the last used service invoice number and prefix from the database
    $serviceinvoiceModel = new ServiceInvoiceModel();
    $lastInvoice = $serviceinvoiceModel->orderBy('service_number', 'DESC')->first();

    if ($lastInvoice) {
        // Extract the numeric part of the last invoice number and increment it
        $lastId = intval(preg_replace('/[^0-9]/', '', $lastInvoice['service_number']));
        $nextId = $lastId + 1;
        $serviceNumber = $prefix . str_pad($nextId, 3, '0', STR_PAD_LEFT);
        while ($serviceinvoiceModel->where('service_number', $serviceNumber)->countAllResults() > 0) {
            $nextId++;
            $serviceNumber = $prefix . str_pad($nextId, 3, '0', STR_PAD_LEFT);
        }
    } else {
        $serviceNumber = $prefix . '001';
    }

    // Pass the generated service invoice number to the view
    $data['serviceNumber'] = $serviceNumber;
    

    // Load your view here with the necessary data
    return view('billingofficer/serviceInvoice/add_service_invoice', $data);
}


public function save_service_invoice()
{
    if ($this->request->getMethod() === 'post') {

        // Get the service number
        $serviceNumber = $this->request->getPost('service_number');

        // Now you have the correct service number, proceed with saving the service invoice
        $serviceInvoiceData = [
            'service_number'    => $serviceNumber,
            'service_date'      => $this->request->getPost('service_date'),
            'mobile_no'         => $this->request->getPost('mobile_no'),
            'cr_name'           => $this->request->getPost('cr_name'),
            'cr_address'        => $this->request->getPost('cr_address'),
            'branch_location'   => $this->request->getPost('branch'),
            'payment_type'      => $this->request->getPost('payment_type'),
            'paid_amount'       => $this->request->getPost('paid_amount'),
            'grand_total'       => $this->request->getPost('grand_total'),
            'status'            => $this->request->getPost('status'),
        ];

        // Save the service invoice data
        $db = db_connect();
        $builder = $db->table('service_invoice');
        $builder->insert($serviceInvoiceData);

        // Check if the insertion was successful
        if (!$builder) {
            die("Failed to insert data into service invoices table.");
        }

        // Use the builder directly to get the inserted ID
        $serviceInvoiceId = $db->insertID();

        // Check if the inserted ID is retrieved successfully
        if (!$serviceInvoiceId) {
            die("Failed to retrieve the inserted ID from invoices table.");
        }

        // Now, proceed with handling service items and prices
        $serviceItems = $this->request->getPost('sr_item');
        $prices = $this->request->getPost('price');

        foreach ($serviceItems as $key => $serviceItem) {
            $serviceModel = new ServiceModel();

            // Check if the service item already exists in the database
            $service = $serviceModel->where('sr_item', $serviceItem)->first();

            if (!$service) {
                // If not, insert it into the services table
                $serviceModel->insert(['sr_item' => $serviceItem]);
                // Retrieve the inserted service data
                $service = $serviceModel->where('sr_item', $serviceItem)->first();
            }

            // Prepare data for inserting into service_items table
            $invoiceServiceData = [
                'sr_invoice_id' => $serviceInvoiceId,
                'service_id'    => $service['id'],
                'sr_item'       => $serviceItem,
                'price'         => $prices[$key],
            ];

            // Insert data into service_items table
            $serviceItemModel = new ServiceItemModel();
            $serviceItemModel->insert($invoiceServiceData);
        }

        // Load the view for managing invoices and pass the invoiceId
       

    if ($this->request->getPost('srprint')) {
        // Get the service number
        $serviceNumber = $this->request->getPost('service_number');

        // Now you have the correct service number, proceed with saving the service invoice
        $serviceInvoiceData = [
            'service_number'    => $serviceNumber,
            'service_date'      => $this->request->getPost('service_date'),
            'mobile_no'         => $this->request->getPost('mobile_no'),
            'cr_name'           => $this->request->getPost('cr_name'),
            'cr_address'        => $this->request->getPost('cr_address'),
            'branch_location'   => $this->request->getPost('branch'),
            'payment_type'      => $this->request->getPost('payment_type'),
            'paid_amount'       => $this->request->getPost('paid_amount'),
            'grand_total'       => $this->request->getPost('grand_total'),
            'status'            => $this->request->getPost('status'),
        ];
       

        // Save the service invoice data
        $db = db_connect();
        $builder = $db->table('service_invoice');
        $builder->insert($serviceInvoiceData);

        // Check if the insertion was successful
        if (!$builder) {
            die("Failed to insert data into service invoices table.");
        }

        // Use the builder directly to get the inserted ID
        $serviceInvoiceId = $db->insertID();

        // Check if the inserted ID is retrieved successfully
        if (!$serviceInvoiceId) {
            die("Failed to retrieve the inserted ID from invoices table.");
        }

        // Now, proceed with handling service items and prices
        $serviceItems = $this->request->getPost('sr_item');
        $prices = $this->request->getPost('price');

        foreach ($serviceItems as $key => $serviceItem) {
            $serviceModel = new ServiceModel();

            // Check if the service item already exists in the database
            $service = $serviceModel->where('sr_item', $serviceItem)->first();

            if (!$service) {
                // If not, insert it into the services table
                $serviceModel->insert(['sr_item' => $serviceItem]);
                // Retrieve the inserted service data
                $service = $serviceModel->where('sr_item', $serviceItem)->first();
            }

            // Prepare data for inserting into service_items table
            $invoiceServiceData = [
                'sr_invoice_id' => $serviceInvoiceId,
                'service_id'    => $service['id'],
                'sr_item'       => $serviceItem,
                'price'         => $prices[$key],
            ];
           

            // Insert data into service_items table
            $serviceItemModel = new ServiceItemModel();
            $serviceItemModel->insert($invoiceServiceData);
        }

        // Load the view for managing invoices and pass the invoiceId
        $model = new ServiceInvoiceModel();
        $data['sr_invoices'] = $model->where('status', 'paid')->findAll();

        return redirect()->to("/srgeneratePdf/{$serviceInvoiceId}")->with('success', 'Invoice saved successfully.');
    }
    else{

   
    $model = new ServiceInvoiceModel();
    $data['sr_invoices'] = $model->where('status', 'paid')->findAll();

    return redirect()->to("billingofficer/manage_service_invoice")->with('success', 'Invoice saved successfully.');
    }
  }
}


public function edit_sr_invoice($id)
{
    $branchModel = new BranchModel();
    $branches = $branchModel->where('status', 'active')->findAll();
    $data['branches'] = $branches;

    $serviceinvoiceModel = new ServiceInvoiceModel();
    $serviceinvoice = $serviceinvoiceModel->find($id);


   

    $serviceItemModel = new serviceItemModel();
    $invoiceServices = $serviceItemModel->where('sr_invoice_id', $id)->findAll();

    $serviceDetails = [];
    foreach ($invoiceServices as $invoiceService) {
        $serviceModel = new ServiceModel();
        $service = $serviceModel->find($invoiceService['service_id']);

        if ($service) {
            $serviceDetails[] = [
                'sr_item' => $service['sr_item'],
                'price' => $invoiceService['price'],
            ];
        }
    }

    $data['serviceinvoice'] = $serviceinvoice;
    $data['serviceDetails'] = $serviceDetails;
    $data['invoiceServices'] = $invoiceServices; // Add this line

    return view('billingofficer/serviceInvoice/edit_sr_invoice', $data);
}

public function update_sr_invoice()
{
    $id = $this->request->getPost('sr_invoice_id');

    $serviceinvoiceData = [
        'service_date' => $this->request->getPost('service_date'),
        'mobile_no' => $this->request->getPost('mobile_no'),
        'cr_name' => $this->request->getPost('cr_name'),
        'cr_address' => $this->request->getPost('cr_address'),
        'payment_type' => $this->request->getPost('payment_type'),
        'paid_amount' => $this->request->getPost('paid_amount'),
        'status' => $this->request->getPost('status'),
    ];

    $db = db_connect();
    $builder = $db->table('service_invoice');
    $builder->where('id', $id)->update($serviceinvoiceData);

    if (!$builder) {
        die("Failed to update data in the invoices table.");
    }

    // Update existing invoice products


    return redirect()->to("billingofficer/manage_service_invoice")->with('success', 'Invoice updated successfully.');
}

public function delete_sr_invoice($id)
{
    // Load the model
    $serviceinvoiceModel = new ServiceInvoiceModel();

    // Check if the invoice with the given ID exists
    $serviceinvoice = $serviceinvoiceModel->find($id);


    // Update the status to 'canceled' instead of deleting
    $serviceinvoiceModel->set('status', 'not_paid')->where('id', $id)->update();

    // Optionally, you can update related records (invoice products, etc.) if needed

    // Redirect to the manage_invoice page with a success message
    return redirect()->to("billingofficer/manage_service_invoice")->with('success', 'Invoice canceled successfully.');
}


public function getServiceItemsByBranch()
{
    try {
        // Get the branch_location from the POST data
        $branch_location = $this->request->getPost('branch_location');

        // Load the required models
        $servicePricesModel = new \App\Models\servicePricesModel();
        $serviceModel = new \App\Models\serviceModel();
        $branchModel = new \App\Models\BranchModel();

        // Validate $branch_location
        if (empty($branch_location)) {
            // Return an error response if $branch_location is empty
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location is required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Build the query to fetch available product names
            $query = $servicePricesModel->select('service.sr_item')
                ->distinct()
                ->join('service', 'service.id = service_prices.sr_id')
                ->where('service_prices.branch_id', $branch_id)
                ->where('service.status', 'available');

            // Fetch results
            $service_items = $query->get()->getResultArray();

            // Return service item as JSON
            return $this->response->setJSON(['service_items' => $service_items]);
        } else {
            // Return an error response if the branch is not found
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        // Return an error response if an exception occurs
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
}



public function getServicePrice()
{
    try {
        $branch_location = $this->request->getPost('branch_location');
        $sr_item = $this->request->getPost('sr_item');

        // Load the required models
        $ServicePricesModel = new \App\Models\ServicePricesModel();
        $branchModel = new \App\Models\BranchModel();
        $ServiceModel = new \App\Models\ServiceModel();

        // Validate branch_location and sr_item
        if (empty($branch_location) || empty($sr_item)) {
            return $this->response->setStatusCode(400)->setJSON(['error' => 'Branch location and service item are required']);
        }

        // Get the branch_id from the branch_location
        $branch = $branchModel->where('branch_location', $branch_location)->first();

        if ($branch) {
            $branch_id = $branch['id'];

            // Fetch product_id from products table using sr_item
            $service = $ServiceModel->where('sr_item', $sr_item)->first();

            if ($service) {
                $sr_id = $service['id'];

                // Build the query to fetch service price
                $query = $ServicePricesModel
                    ->select('price')
                    ->where('branch_id', $branch_id)
                    ->where('sr_id', $sr_id)
                    ->limit(1);

                // Fetch the result
                $result = $query->get()->getRow();

                if ($result) {
                    $price = $result->price;
                    return $this->response->setJSON(['price' => $price]);
                } else {
                    return $this->response->setStatusCode(404)->setJSON(['error' => 'Product price not found']);
                }
            } else {
                return $this->response->setStatusCode(404)->setJSON(['error' => 'Product not found']);
            }
        } else {
            return $this->response->setStatusCode(404)->setJSON(['error' => 'Branch not found']);
        }
    } catch (\Exception $e) {
        return $this->response->setStatusCode(500)->setJSON(['error' => $e->getMessage()]);
    }
} 
        










public function fetch_sr_invoice_details()
{
    $request = \Config\Services::request();
    $srinvoiceId = $request->getPost('srinvoiceId');

    $serviceinvoiceModel = new \App\Models\ServiceInvoiceModel();
    $srinvoiceDetails = $serviceinvoiceModel->getInvoiceWithService($srinvoiceId);


    return $this->response->setJSON($srinvoiceDetails);
}


public function service_invoice_report()
    {
        return view('billingofficer/serviceInvoice/service_report_form');
    }

    public function displaySrInvoiceReport()
    {
        $ServiceInvoiceModel = new ServiceInvoiceModel();
    
    $branchModel = new BranchModel();

    // Get the selected date range (you need to get this from your form)
    $startDate = $this->request->getPost('start_date');
    $endDate = $this->request->getPost('end_date');

    // Get the branch information of the logged-in user
     $loggedInBranch = session()->get('loggedInUser')['branch'];
     
       
    
        // Fetch invoices between the selected date range with status "paid"
        $invoices = $ServiceInvoiceModel
            ->where('status', 'paid')
            ->where('service_date >=', $startDate)
            ->where('service_date <=', $endDate)
            ->where('branch_location', $loggedInBranch)
            ->findAll();
    
   
        // Calculate the overall total of paid_amount
        $overallTotal = array_sum(array_column($invoices, 'grand_total'));
    
        // Pass the data to the view, including start and end dates
        $data = [
            'invoices' => $invoices,
            'overallTotal' => $overallTotal,
            'startDate' => $startDate,  // Add this line
            'endDate' => $endDate,      // Add this line
        ];
    
        return view('billingofficer/serviceinvoice/invoice_service_report', $data);
    }

}