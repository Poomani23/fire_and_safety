<?php

namespace App\Controllers;
use App\Models\ServiceModel;
use App\Models\ServicePricesModel;

use App\Models\BranchModel;
use App\Models\categoryModel;


use CodeIgniter\Controller;

class ServiceController extends Controller
{
     protected $serviceModel;
    protected $servicePricesModel;
    protected $branchModel;

    public function __construct()
    {
        $this->serviceModel = new ServiceModel();
        $this->servicePricesModel = new ServicePricesModel();
        $this->branchModel = new BranchModel();
    }
// manage function


public function index()
{
    $services = $this->serviceModel->getAllServices();
    $branchLocations = $this->servicePricesModel->getAllBranchLocations();

    // Pass both services and servicePricesModel to the view
    $data = [
        'services' => $services,
        'branchLocations' => $branchLocations,
        'servicePricesModel' => $this->servicePricesModel,
    ];

    // Load the view with data
    return view('service/manage_service', $data);
}

public function add_service()
{
    $isAdmin = true; // Set to true for admin

    $data = [
        'isAdmin' => $isAdmin,
    ];
 $branchModel = new BranchModel();
    $branches = $branchModel->where('status', 'active')->findAll();
    $data['branches'] = $branches;


        // Pass the $branches data to the view
      
        $categoryModel = new CategoryModel();

        $data['categories'] = $categoryModel->where('status', 'available')->findAll();
    return view('service/add_service',$data);
}
public function store()
{
    $serviceModel = new ServiceModel();

    // Retrieve input data from the service form
    $category = $this->request->getPost('category');
    $srItem = $this->request->getPost('sr_item');
    $description = $this->request->getPost('description');
    $status = $this->request->getPost('status');

    // Insert data into the service table
    $serviceData = [
        'category' => $category,
        'sr_item' => $srItem,
        'description' => $description,
        'status' => $status,
    ];  


    $serviceModel->insert($serviceData);

    // Get the last inserted service ID
    $srId = $serviceModel->getInsertID();

    // Retrieve input data from the service_prices form
    $servicePricesModel = new ServicePricesModel();

    // Loop through branches and insert data into the service_prices table
    foreach ($this->request->getPost('service_price') as $branchId => $price) {
        $branchModel = new BranchModel();
        $branch = $branchModel->find($branchId);

        // Insert data into the service_prices table
        $servicePriceData = [
            'sr_id' => $srId,
             'sr_item' => $srItem,
            'branch_id' => $branchId,
            'branch_location' => $branch['branch_location'],
            'price' => $price,
        ];
        $servicePricesModel->insert($servicePriceData);
    }

    // Redirect to a success page or any other desired action
     return redirect()->to(base_url('manage_service'));
}

// ServiceController.php

// ServiceController.php

public function edit_service($serviceId)
    {
        // Fetch service data by ID
        $serviceModel = new ServiceModel();
        $service = $serviceModel->getServiceById($serviceId);

        // Fetch prices for the service
        $servicePricesModel = new ServicePricesModel();
        $currentPrices = $servicePricesModel->getPricesByServiceId($serviceId);

        // Fetch other necessary data (categories, branches, etc.)
        $categoryModel = new CategoryModel();
        $categories = $categoryModel->where('status', 'available')->findAll();

        $branchModel = new BranchModel();
        $branches = $branchModel->where('status', 'active')->findAll();

        // Define the getBranchPrice logic
        $getBranchPrice = function ($currentPrices, $branchId) {
            foreach ($currentPrices as $price) {
                if ($price['branch_id'] == $branchId) {
                    return $price['price'];
                }
            }
            return 0; // Default value if no price is found for the branch
        };

        // Pass data to the view
        $data = [
            'service' => $service,
            'categories' => $categories,
            'branches' => $branches,
            'currentPrices' => $currentPrices,
            'getBranchPrice' => $getBranchPrice,
        ];

        // Load the edit_service view with data
        return view('service/edit_service', $data);
    }

 
public function update_service($serviceId)
{
    // Validate form data if needed

    // Retrieve input data from the service form
    $category = $this->request->getPost('category');
    $srItem = $this->request->getPost('sr_item');
    $description = $this->request->getPost('description');
    $status = $this->request->getPost('status');

    // Update data in the service table
    $serviceModel = new ServiceModel();
    $serviceData = [
        'category' => $category,
        'sr_item' => $srItem,
        'description' => $description,
        'status' => $status,
    ];
    
    $serviceModel->update($serviceId, $serviceData);

    // Retrieve input data for prices from the form
    $branchPrices = $this->request->getPost('service_price');

    // Update prices in the service_prices table
    $servicePricesModel = new ServicePricesModel();
    $branchModel = new BranchModel();

    foreach ($branchPrices as $branchId => $price) {
        // Check if a record exists for the branch and service ID
        $existingPrice = $servicePricesModel->where('sr_id', $serviceId)
                                             ->where('branch_id', $branchId)
                                             ->first();

        if ($existingPrice) {
            // If a record exists, update the price
            $servicePricesModel->update($existingPrice['id'], ['price' => $price]);
        } else {
                $branchLocation = $branchModel->getBranchLocationById($branchId);
            // If no record exists, insert a new record
            $servicePricesModel->insert([
                'sr_id' => $serviceId,
                'sr_item' => $srItem, // Assuming you want to store the service item in the service_prices table
                'branch_id' => $branchId,
                'branch_location' => $branchLocation, // You need a method to get branch location by ID
                'price' => $price,
            ]);
        }
    }

    // Redirect to a success page or any other desired action
    return redirect()->to(base_url('manage_service'));
}
public function delete_service($serviceId)
    {
        // Fetch service data by ID
        $serviceModel = new ServiceModel();
        $service = $serviceModel->find($serviceId);

        if (!$service) {
            // Handle case where service with the given ID is not found
            return redirect()->to(base_url('manage_service'))->with('error', 'Service not found.');
        }

        // Update status to "unavailable"
        $serviceModel->update($serviceId, ['status' => 'unavailable']);

        // Redirect to a success page or any other desired action
        return redirect()->to(base_url('manage_service'))->with('success', 'Service status updated to unavailable.');
    }

}
