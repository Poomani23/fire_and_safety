<?php

namespace App\Controllers;
use App\Models\RoleModel;
use App\Models\BranchModel;


use CodeIgniter\Controller;

class RoleController extends Controller
{
    
// manage function
public function index()
{
    $isAdmin = true; // Set to true for admin
    $data = [
        'isAdmin' => $isAdmin,
    ];
    $roleModel = new RoleModel();
    $data['users'] = $roleModel->where('status', 'active')->findAll();
    return view('role/role_view',$data);
}
// add function form page
public function add_role()
{
    $branchModel = new BranchModel();

    $data['branches'] = $branchModel->where('status', 'active')->findAll();
    

    return view('role/add_role',$data);
}
// insert data into database
public function store() {
    $roleModel = new RoleModel();
    $data = [
        'fname' => $this->request->getPost('fname'),
        'lname' => $this->request->getPost('lname'),
        'username' => $this->request->getPost('username'),
        'emp_id' => $roleModel->generateUniqueEmployeeID($this->request->getPost('role')),
        'role' => $this->request->getPost('role'),
        'email' => $this->request->getPost('email'),
        'phone' => $this->request->getPost('phone'),
        'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
        'branch' => $this->request->getPost('branch'),
        'status' => $this->request->getPost('status'),
    ];
    $roleModel->insert($data);
    return $this->response->redirect(site_url('/manage_role '));
}
   
// view single user
public function edit($id)
{
    $roleModel = new RoleModel();
    $data['role'] = $roleModel->find($id);

    // Fetch the selected branch associated with the role being edited
    $selectedBranch = !empty($data['role']['branch']) ? $data['role']['branch'] : '';

    $branchModel = new BranchModel();
    $data['branches'] = $branchModel->where('status', 'active')->findAll(); 

    // Assign $selectedBranch to the data array
    $data['selectedBranch'] = $selectedBranch;

    // Load the view with the data
    return view('role/edit_role',$data);

}


// update user data

public function update($id)
{
    $roleModel = new RoleModel();
    $data = [
        'fname' => $this->request->getPost('fname'),
        'lname' => $this->request->getPost('lname'),
        'username' => $this->request->getPost('username'),
        'emp_id' => $roleModel->generateUniqueEmployeeID($this->request->getPost('role')),
        'role' => $this->request->getPost('role'),
        'email' => $this->request->getPost('email'),
        'phone' => $this->request->getPost('phone'),
        'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
        'branch' => $this->request->getPost('branch'),
        'status' => $this->request->getPost('status'),
    ];

    // Update the branch details in the database
    $roleModel->update($id, $data);

    // Redirect to the manage branches page after updating
    return redirect()->to(base_url('manage_role'));
}



// delete user
public function delete($id)
{
    $roleModel = new RoleModel();
   
   
    $data = ['status' => 'inactive'];
    $roleModel->update($id, $data);
    return redirect()->to(base_url('/manage_role'));
}

}