<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ProductModel;
use App\Models\BranchModel;
use App\Models\ProductPricesModel;
use App\Models\categoryModel;




class ProductController extends BaseController
{    
    protected $branchModel;
    protected $productModel;
    protected $productPricesModel;

    public function __construct()
    {
        $this->branchModel = new BranchModel();
        $this->productModel = new ProductModel();
        $this->productPricesModel = new ProductPricesModel();

    }
    // Inside your ProductController
    
    public function index()
    {
        // Fetch products
        $products = $this->productModel->getAllProducts();

        $pricesData = [];

        // Loop through products to get prices by product ID
        foreach ($products as $product) {
            $productId = $product['id'];
            $pricesData[$productId] = $this->productPricesModel->getPricesByProductId($productId);
        }
       
       
       
       
        // Fetch all branch locations
        $branchLocations = $this->branchModel->getAllBranchLocations(); 
       
    
        // Prepare an array to hold location data for each branch
        $locationData = [];
        foreach ($branchLocations as $branch) {
            $branchId = $branch['id'];
            $locationData[$branchId] = $this->productPricesModel->getLocationByBranchId($branchId);
        }
       
        
        // Combine data into one array
        $data = [
            'products' => $products,
            'pricesData' => $pricesData,
            'branchLocations' => $branchLocations // Pass branch locations to the view
        ];
    
        // Load the view with data
        return view('product/manage_product', $data);
    }
    
   
    

    




    public function add_product() {
        $branchModel = new BranchModel();
        
        // Fetch only active branches from the database
        $branches = $branchModel->where('status', 'active')->findAll();
        
        // Pass the $branches data to the view
        $data['branches'] = $branches;

        $categoryModel = new CategoryModel();

        $data['categories'] = $categoryModel->where('status', 'available')->findAll();
        
        return view('product/add_product', $data);
      
           
    }
    public function save_product() {
        // Check if the form is submitted
        if ($this->request->getMethod() === 'post') {
            // Get form data
            $category = $this->request->getPost('category');
            $product_name = $this->request->getPost('product_name');
            $description = $this->request->getPost('description');
            $status = $this->request->getPost('status');
            $product_prices = $this->request->getPost('product_price');
            $product_quantity = $this->request->getPost('product_quantity');
           
            // Handling file upload for product image
            $product_image = $this->request->getFile('product_image');
    
            if ($product_image && $product_image->getError() !== UPLOAD_ERR_OK) {
                // Handle upload error with an alert
                return redirect()->to(base_url('add_product'))->with('error', 'Upload error: ' . $product_image->getErrorString());
            }
    
            // Initialize variable to store the new image name
            if ($product_image && $product_image->isValid() && !$product_image->hasMoved()) {
                if ($product_image->getClientMimeType() === 'image/jpeg' || $product_image->getClientMimeType() === 'image/png') {
                    // Generate a new unique name for the image to avoid conflicts
                    $newImageName = $product_image->getRandomName();
                   
                    // Validate and move the uploaded file to a designated directory
                    $destination = 'public/uploads';
                 
                   
        
                    if ($product_image->move($destination, $newImageName)) {
                        // Image successfully uploaded and moved
                        $productModel = new ProductModel();
                        $productPricesModel = new ProductPricesModel();
    
                        // Get the last product's ID from the database
                        $lastProduct = $productModel->orderBy('id', 'DESC')->first();
                        if ($lastProduct) {
                            // Extract the numeric part of the ID and increment it
                            $lastId = intval(substr($lastProduct['product_id'], strlen('product'))); // Extract the numeric part
                            $productId = 'product' . str_pad($lastId + 1, 3, '0', STR_PAD_LEFT); // Increment and format to 'productXXX'
                        } else {
                            $productId = 'product001';
                        }
    
                        // Prepare data for insertion into the database
                        $data = [
                            'product_id' => $productId,
                            'category' => $category,
                            'product_image' => $newImageName,
                            'product_name' => $product_name,
                            'description' => $description,
                            'status' => $status
                        ];
                        $product_id = $productModel->saveProduct($data);
                       
                        
                      

                        if ($product_id && is_array($product_prices) && is_array($product_quantity)) {
                            // Save product prices associated with the product using ProductPricesModel
                            $productPricesModel->saveProductPrices($product_id, $product_prices, $product_quantity);
                        }
                        
                        
    
                        // Redirect after successful save
                        return redirect()->to(base_url('manage_product'))->with('success', 'Product added successfully');
                    } else {
                        // Failed to move the image with an alert
                        $errorMessage = $product_image->getError() !== null ? $product_image->getErrorString() : 'Unknown error';
                        error_log('Failed to move the image: ' . $errorMessage);
                        return redirect()->to(base_url('add_product'))->with('error', 'Failed to move the image');
                    }
                } else {
                    // Handle unsupported file format with an alert
                    return redirect()->to(base_url('add_product'))->with('error', 'Uploaded file is not a supported image format (JPEG/PNG)');
                }
            } else {
                // Image upload conditions not met with an alert
                return redirect()->to(base_url('add_product'))->with('error', 'Image upload conditions not met');
            }
        }
    
        // Handle cases where the form submission method is not POST
        // Redirect or show an error message as needed
        return redirect()->to(base_url('add_product'))->with('error', 'Invalid request');
    }
   
    public function editProduct($productId)
{
    // Fetch product details by ID
    $product = $this->productModel->find($productId);

    // Fetch prices for the product by product ID
    $pricesData = $this->productPricesModel->getPricesByProductId($productId);

    // Fetch all branch locations
    $branchLocations = $this->branchModel->getAllBranchLocations();

    // Prepare an array to hold location data for each branch
    $locationData = [];
    foreach ($branchLocations as $branch) {
        $branchId = $branch['id'];
        $locationData[$branchId] = $this->productPricesModel->getLocationByBranchId($branchId);
    }
    $categoryModel = new CategoryModel();

    $data['categories'] = $categoryModel->where('status', 'available')->findAll();
    // Combine data into one array
    $data['product'] = $product;
    $data['pricesData'] = $pricesData;
    $data['branchLocations'] = $branchLocations;
    $data['locationData'] = $locationData;

    // Load the view for editing a specific product with the fetched data
    return view('product/edit_product', $data);
}




    
    
    
    // Method to fetch product details, prices, and branch locations
    protected function fetchProductPricesAndBranches($productId)
    {
        // Fetch product details by ID
        $data['product'] = $this->productModel->find($productId);
    
        // Fetch prices for the product by product ID
        $data['pricesData'] = $this->productPricesModel->getPricesByProductId($productId);
    
        // Extract branches and prices for the specific product from pricesData
        $data['branchesPrices'] = $data['pricesData'] ?? [];
    
        // Load other necessary data into the $data array if needed
    
        // Return the data array
        return $data;
    }
    
    

    
    public function updateproduct($product_id)
    {
        if ($this->request->getMethod() === 'post') {
            $category = $this->request->getPost('category');
            $product_name = $this->request->getPost('product_name');
            $description = $this->request->getPost('description');
            $status = $this->request->getPost('status');
    
            // Get the prices array submitted via POST
            $product_prices = $this->request->getPost();
           
            
             // Get the quantities array submitted via POST
            $product_quantity = $this->request->getPost();
           
    
            // Filter the prices to extract only the price data (assuming they were named like 'price_branchId')
            $product_prices_filtered = [];
            foreach ($product_prices as $key => $value) {
                if (strpos($key, 'price_') === 0) {
                    $branch_id = substr($key, strlen('price_')); // Extract branch ID
                    $product_prices_filtered[$branch_id] = $value;
                }
            }
// Filter the quantities to extract only the quantity data (assuming they were named like 'quantity_branchId')
$product_quantity_filtered = [];
foreach ($product_quantity as $key => $value) {
    if (strpos($key, 'quantity_') === 0) {
        $branch_id = substr($key, strlen('quantity_')); // Extract branch ID
        $product_quantity_filtered[$branch_id] = $value;
    }
}


            // Now $product_prices_filtered contains an array where keys are branch IDs and values are prices
            // You can use this array to update prices in the database
    
            $productModel = new ProductModel();
    
            // Handling file upload for product image
            $product_image = $this->request->getFile('product_image');
    
            if ($product_image && $product_image->isValid() && !$product_image->hasMoved()) {
                if ($product_image->getClientMimeType() === 'image/jpeg' || $product_image->getClientMimeType() === 'image/png') {
                    $newImageName = $product_image->getRandomName();
                    $destination = 'public/uploads';
    
                    if ($product_image->move($destination, $newImageName)) {
                        $data = [
                            'category' => $category,
                            'product_image' => $newImageName,
                            'product_name' => $product_name,
                            'description' => $description,
                            'status' => $status
                        ];
    
                        $productModel->updateProduct($product_id, $data);
    
                        $productPricesModel = new ProductPricesModel();
                        $productPricesModel->updateProductPrices($product_id, $product_prices_filtered);
                       
                        $productPricesModel = new ProductPricesModel();
                        $productPricesModel->updateProductQuantity($product_id, $product_quantity_filtered);
                
    
                        return redirect()->to(base_url('manage_product'))->with('success', 'Product updated successfully');
                    } else {
                        return redirect()->to(base_url('edit_product/' . $product_id))->with('error', 'Failed to move the image');
                    }
                } else {
                    return redirect()->to(base_url('edit_product/' . $product_id))->with('error', 'Uploaded file is not a supported image format (JPEG/PNG)');
                }
            } else {
                // If no new image is uploaded, update other product details
                $data = [
                    'category' => $category,
                    'product_name' => $product_name,
                    'description' => $description,
                    'status' => $status
                ];
    
                $productModel->updateProduct($product_id, $data);
    
                $productPricesModel = new ProductPricesModel();
                $productPricesModel->updateProductPrices($product_id, $product_prices_filtered);
                $productPricesModel = new ProductPricesModel();
                $productPricesModel->updateProductQuantity($product_id, $product_quantity_filtered);
    
                return redirect()->to(base_url('manage_product'))->with('success', 'Product updated successfully');
            }
        }
    
        return redirect()->to(base_url('manage_product'))->with('error', 'Invalid request');
    }
    
    
    public function deleteProduct($productId)
    {
        $ProductModel = new ProductModel();
        
       
        $data = ['status' => 'unavailable'];
        $ProductModel->update($productId, $data);
        return redirect()->to(base_url('manage_product'));
    }
 

    

   


}