<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\InvoiceModel;
use App\Models\InvoiceProductModel;
use App\Models\ProductModel;

use App\Controllers\BaseController;
use Dompdf\Dompdf;
use Dompdf\Options;

class PdfController extends BaseController
{

    public function generatePdf($invoiceId)
    {

        helper('custom');
        // Fetch customer and invoice details
        $data = $this->fetchCustomerAndInvoiceDetails($invoiceId);

        if (!$data) {
            // Handle the case where the data couldn't be fetched
            return;
        }

        // Load the HTML template
        $html = view('pdf/invoice_pdf', $data);

        // Setup Dompdf
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isPhpEnabled', true);

        $dompdf = new Dompdf($options);

        // Load HTML content into Dompdf
        $dompdf->loadHtml($html);

        // Set paper size (A4)
        $dompdf->setPaper('A4', 'portrait');

        // Render PDF (first pass to get the total pages)
        $dompdf->render();

        // Output PDF (save to file, download, etc.)
        $dompdf->stream("invoice.pdf", array("Attachment" => false));
    }

   private function fetchCustomerAndInvoiceDetails($invoiceId)
{
    // Load necessary models
    $invoiceModel = new InvoiceModel();
    $invoiceProductModel = new InvoiceProductModel();
    $productModel = new ProductModel(); // Add this line to load the ProductModel

    // Fetch invoice details
    $invoice = $invoiceModel->find($invoiceId);

    // Check if the invoice exists
    if (!$invoice) {
        // Handle the case where the invoice doesn't exist
        return null;
    }

    // Fetch invoice products with related product information using a join
    $invoiceProducts = $invoiceProductModel
        ->select('invoice_products.*, products.product_name') // Add the fields you need
        ->join('products', 'products.id = invoice_products.product_id')
        ->where('invoice_id', $invoiceId)
        ->findAll();

    // Calculate grand total
    $grandTotal = array_reduce($invoiceProducts, function ($carry, $product) {
        return $carry + $product['total'];
    }, 0);

    // Prepare data structure
    $data = [
        'customerDetails' => [
            'name' => $invoice['name'],
            'address' => $invoice['address'],
            'branch' => $invoice['branch'],
        ],
        'invoice' => [
            'invoice_number' => $invoice['invoice_number'],
            'invoice_date' => $invoice['invoice_date'],
        ],
        'invoices' => $invoiceProducts,
        'grandTotal' => $grandTotal,
    ];

    return $data;
}
// Define the convertToWords function



}
