<?php

namespace App\Controllers;
use App\Models\BranchModel;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\SimplePager;


class Branch extends BaseController
{
    public function index()
    {

        $isAdmin = true; 

    $data = [
        'isAdmin' => $isAdmin,
    ];
        return view('branch/manage_branch',$data);
    }

    public function add_branch()
    {

        $isAdmin = true; 

    $data = [
        'isAdmin' => $isAdmin,
    ];
        return view('branch/add_branch',$data);
    }

    public function save()
{
    $branchModel = new BranchModel(); 

    $data = [
        'branch_name' => $this->request->getPost('branch_name'),
        'branch_location' => $this->request->getPost('branch_location'),
        'status' => $this->request->getPost('status'),
        'isAdmin' => true,
    ];

    $branchModel->insert($data);

    return redirect()->to(base_url('manage_branch'));


}
   public function manage()
{
    $branchModel = new BranchModel();

    // Retrieve branches with 'active' status
    $data['branches'] = $branchModel->where('status', 'active')->findAll();

    return view('branch/manage_branch', $data);
}

    public function edit($id)
    {
        $branchModel = new BranchModel();
        $data['branch'] = $branchModel->find($id); // Fetch branch details by ID

        // Load the edit view with branch details
        return view('branch/edit_branch', $data);
    }
     public function update($id)
    {
        $branchModel = new BranchModel();
        $data = [
            'branch_name' => $this->request->getPost('branch_name'),
            'branch_location' => $this->request->getPost('branch_location'),
            'status' => $this->request->getPost('status')
        ];

        // Update the branch details in the database
        $branchModel->update($id, $data);

        // Redirect to the manage branches page after updating
        return redirect()->to(base_url('manage_branch'));
    }

    public function delete($id)
{
    $branchModel = new BranchModel();
    
   
    $data = ['status' => 'inactive'];
    $branchModel->update($id, $data);
    return redirect()->to(base_url('manage_branch'));
}


}
