<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Controller;

class UserController extends Controller
{
    

public function showlogin()
{
    // Load the view with the registration form
    return view('layout/login');
}

public function showregister()
{
    // Load the view with the registration form
    return view('layout/register');
}
public function showinvalidlogin()
{
    // Load the view with the registration form
    return view('layout/invalid_login');
}

public function adminLogin()
{
    $username = $this->request->getPost('username'); 
    $password = $this->request->getPost('password'); 

    if ($username === 'admin' && $password === 'admin@123') {
        
        session_start();
        $_SESSION['isAdminLoggedIn'] = true;
        $_SESSION['user_name'] = $username;

        return redirect()->to(base_url('dashboard'));
    } else {
        return redirect()->to(base_url('invalid_login'));
    }
}



}
