<?php


namespace App\Controllers;


use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\BranchModel;
use App\Models\CategoryModel;
use App\Models\ServiceModel;
use App\Models\ProductModel;
use App\Models\InvoiceModel;
use App\Models\InvoiceProductModel;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceItemModel;


class DashboardController extends BaseController
{
    public function index()
{
    $userModel = new UserModel();
    $loggedInUserId = session()->get('loggedInUser');
    $userInfo = $userModel->find($loggedInUserId);

    $data = [
        'title' => ' User Dashboard',
        'userInfo' => $userInfo,
        'isAdmin' => false, // Set isAdmin to false for non-admin user dashboard
    ];

    return view('layout/user_dashboard', $data);
}


   public function admin_dashboard()
{
    $branchModel = new BranchModel();
    $data['totalActiveBranches'] = $branchModel->countActiveBranches();

    $categoryModel = new CategoryModel();
    $data['totalAvailableCategories'] = $categoryModel->countAvailableCategories();

    $serviceModel = new ServiceModel();
    $data['totalAvailableservices'] = $serviceModel->countAvailableservices();

    $productModel = new ProductModel();
    $data['totalAvailableProducts'] = $productModel->countAvailableProducts();

    $invoiceModel = new InvoiceModel();
    $data['todayProductInvoices'] = $invoiceModel->countTodayProductInvoices();
    $data['monthlyProductInvoices'] = $invoiceModel->countMonthlyProductInvoices();

    $serviceinvoiceModel = new serviceInvoiceModel();
    $data['todayServiceInvoices'] = $serviceinvoiceModel->countTodayServiceInvoices();
    $data['monthlyServiceInvoices'] = $serviceinvoiceModel->countMonthlyServiceInvoices();

       
    return view('layout/admin_dashboard',$data);
}

}