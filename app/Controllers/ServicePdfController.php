<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceItemModel;
use App\Models\ServiceModel;
use App\Models\ServicePricesModel;

use App\Controllers\BaseController;
use Dompdf\Dompdf;
use Dompdf\Options;

class ServicePdfController extends BaseController
{

    public function srgeneratePdf($srinvoiceId)
    {

        helper('custom');
        // Fetch customer and invoice details
        $data = $this->fetchCustomerAndSrInvoiceDetails($srinvoiceId);

        if (!$data) {
            // Handle the case where the data couldn't be fetched
            return;
        }

        // Load the HTML template
        $html = view('pdf/sr_invoice_pdf', $data);

        // Setup Dompdf
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isPhpEnabled', true);

        $dompdf = new Dompdf($options);

        // Load HTML content into Dompdf
        $dompdf->loadHtml($html);

        // Set paper size (A4)
        $dompdf->setPaper('A4', 'portrait');

        // Render PDF (first pass to get the total pages)
        $dompdf->render();

        // Output PDF (save to file, download, etc.)
        $dompdf->stream("srinvoice.pdf", array("Attachment" => false));
    }

   private function fetchCustomerAndSrInvoiceDetails($srinvoiceId)
{
    // Load necessary models
    $serviceinvoiceModel = new ServiceInvoiceModel();
    $serviceitemModel = new ServiceItemModel();
    $ServiceModel = new ServiceModel(); // Add this line to load the ProductModel

    // Fetch invoice details
    $srinvoice = $serviceinvoiceModel->find($srinvoiceId);

    // Check if the invoice exists
    if (!$srinvoice) {
        // Handle the case where the invoice doesn't exist
        return null;
    }

    // Fetch invoice products with related product information using a join
    $invoiceService = $serviceitemModel
        ->select('service_item.*, service.sr_item') // Add the fields you need
        ->join('service', 'service.id = service_item.service_id')
        ->where('sr_invoice_id', $srinvoiceId)
        ->findAll();
        

    // Calculate grand total
    $grandTotal = array_reduce($invoiceService, function ($carry, $service) {
        return $carry + $service['price'];
    }, 0);
    

    // Prepare data structure
    $data = [
           
        
        'srinvoice' => [
            'service_number' => $srinvoice['service_number'],
            'service_date' => $srinvoice['service_date'],
            'name' => $srinvoice['cr_name'],
            'address' => $srinvoice['cr_address'],
            'branch_location' => $srinvoice['branch_location'],
        ],

        'invoiceService' => $invoiceService,
        'grandTotal' => $grandTotal,
    ];

    return $data;
}
// Define the convertToWords function



}
