<?php
namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class BillingOfficerFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // Check if the user is logged in and has the manager role
        $loggedInUser = session()->get('loggedInUser');

        if (!isset($loggedInUser['role']) || $loggedInUser['role'] !== 'billingofficer') {
            // Set flash message indicating the user must be logged in
            session()->setFlashdata('error', 'You must be logged in  to access the dashboard.');

            // Redirect to a different route (e.g., login page)
            return redirect()->to('login');
        }

        // User is a manager, allow access
        return;
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // No action needed after filtering
    }
}
