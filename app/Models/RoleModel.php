<?php

namespace App\Models;

use CodeIgniter\Model;

class RoleModel extends Model
{
    protected $table = 'role';
    protected $primaryKey = 'id';
    protected $allowedFields = ['fname', 'lname', 'username', 'emp_id', 'role', 'email', 'phone', 'password', 'branch', 'status'];

    // Corrected method name
    public function generateUniqueEmployeeID($role)
    {
        $query = $this->select('emp_id')
        ->where('role', $role)
        ->orderBy('emp_id', 'DESC')
        ->limit(1)
        ->get();

    if ($query->getNumRows() > 0) {
        $lastID = $query->getRow()->emp_id;
        $numericPart = (int) substr($lastID, strlen($role));
        $newID = $role . str_pad($numericPart + 1, 3, '0', STR_PAD_LEFT);
    } else {
        $newID = $role . '001';
    }

    return $newID;
}
}
