<?php

namespace App\Models;

use CodeIgniter\Model;

class InvoiceProductModel extends Model
{
    protected $table = 'invoice_products';
    protected $primaryKey = 'id';
    protected $allowedFields = ['invoice_id', 'product_id', 'quantity', 'price', 'total'];

    // Add any other necessary configurations
}
