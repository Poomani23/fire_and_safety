<?php

namespace App\Models;

use CodeIgniter\Model;

class InvoiceModel extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id'; // Assuming the primary key is 'invoice_id'

    protected $allowedFields = [
        'invoice_number',
        'invoice_date',
        'mobile_no',
        'name',
        'address',
        'branch',
        'paid_amount',
        'grand_total',
        'payment_type',
        'status'
    ];

    public function getInsertID($reset = true)
{
    return parent::getInsertID($reset);
}
   // InvoiceModel.php
// InvoiceModel.php
// public function getInvoiceWithProducts($invoiceId)
// {
//     $invoice = $this->find($invoiceId);

//     if ($invoice) {
//         $invoiceProductModel = new \App\Models\InvoiceProductModel();
//         $products = $invoiceProductModel
//             ->select('products.product_name as product_name', 'invoice_products.quantity', 'invoice_products.price')
//             ->join('products', 'products.id = invoice_products.product_id')
//             ->where('invoice_id', $invoiceId)
//             ->findAll();

//         $invoice['products'] = $products;

//         return $invoice;
//     }

//     return null;
// }
// InvoiceModel.php


  
// InvoiceModel.php
// InvoiceModel.php
// InvoiceModel.php
// InvoiceModel.php
        public function getInvoiceWithProducts($invoiceId)
    {
        $invoice = $this->find($invoiceId);

        if ($invoice) {
            $invoiceProductModel = new \App\Models\InvoiceProductModel();
            $invoiceProducts = $invoiceProductModel
                ->select('invoice_products.quantity, invoice_products.price, invoice_products.total, products.product_name')
                ->join('products', 'products.id = invoice_products.product_id')
                ->where('invoice_id', $invoiceId)
                ->findAll();

            $invoice['invoice_products'] = $invoiceProducts;

            return $invoice;
        }

        return null;
    }



    public function countTodayProductInvoices()
    {
        $todayDate = date('Y-m-d');

        // Fetch today's invoices with status 'paid'
        $todayInvoices = $this
            ->where('status', 'paid')
            ->where('invoice_date', $todayDate)
            ->findAll();

        // Calculate the sum of grand_total for today's invoices
        $todayProductSales = array_sum(array_column($todayInvoices, 'grand_total'));

        return $todayProductSales; // Return the calculated value
    }


    public function countMonthlyProductInvoices()
    {
        $firstDayOfMonth = date('Y-m-01');
        $lastDayOfMonth = date('Y-m-t');

        // Fetch monthly invoices with status 'paid'
        $monthlyInvoices = $this
            ->where('status', 'paid')
            ->where('invoice_date >=', $firstDayOfMonth)
            ->where('invoice_date <=', $lastDayOfMonth)
            ->findAll();

        // Calculate the sum of grand_total for monthly invoices
        $monthlyProductSales = array_sum(array_column($monthlyInvoices, 'grand_total'));

        return $monthlyProductSales; // Return the calculated value
    }

}
