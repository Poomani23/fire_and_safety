<?php

namespace App\Models;

use CodeIgniter\Model;



    class ServiceModel extends Model
{
    protected $table = 'service';
    protected $primaryKey = 'id';
    protected $allowedFields = ['category', 'sr_item', 'description','status'];

     

    public function getAllServices()
    {
        
        return $this->where('status', 'available')->findAll(); 
    }

     public function countAvailableServices()
    {
        return $this->where('status', 'available')->countAllResults();
    }
 public function getServiceById($serviceId)
    {
        return $this->find($serviceId);
    }
    
}
   

