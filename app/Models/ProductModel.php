<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model {
   
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $allowedFields = ['product_id', 'category', 'product_image', 'product_name',  'description', 'status'];
    public function getProductDetails($productId)
    {
        // Retrieve product details by product ID
        return $this->find($productId);
    }
    public function saveProduct($data) {
        // Insert the product data into the 'products' table
        return $this->insert($data);
    }
    public function getAllProducts()
    {
        // Assuming 'products' is the name of your products table
        return $this->where('status', 'available')->findAll();  // Adjust this according to your table name
    }
    public function updateProduct($productId, $data)
    {
        $this->where('id', $productId)
            ->set($data)
            ->update();
    }
    public function countAvailableProducts()
    {
        return $this->where('status', 'available')->countAllResults(); // Count categories with 'available' status
    }
     
}
