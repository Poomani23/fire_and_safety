<?php
namespace App\Models;

use CodeIgniter\Model;


class ProductPricesModel extends Model
{
    protected $table = 'product_prices'; // Set the table name

    protected $primaryKey = 'id'; // Define the primary key

    protected $allowedFields = ['product_id', 'branch_id', 'price','quantity']; // Define the fields that can be manipulated

    // Save product prices associated with a product
    public function saveProductPrices($product_id, $product_prices, $product_quantity)
    {
        foreach ($product_prices as $branch_id => $price) {
            if (isset($product_quantity[$branch_id])) {
                $quantity = $product_quantity[$branch_id]; // Retrieve quantity for the current branch
    
                $this->insert([
                    'product_id' => $product_id,
                    'branch_id' => $branch_id,
                    'price' => $price,
                    'quantity' => $quantity // Insert the quantity along with price
                ]);
            } else {
                // Log or handle the case when $product_quantities[$branch_id] is null or missing
                echo "Quantity not found for branch ID: $branch_id";
                // You might want to throw an exception, log an error, or handle it as per your application's logic
            }
        }
    }
    
    
    public function getPricesByProductId($productId)
    {
        return $this->where('product_id', $productId)->findAll();
       
    }

    
    
   
public function getLocationByBranchId($branchId) {
    
    return $this->db->table('branch')->where('id', $branchId)->get();
}
public function updateProductPrices($product_id, $product_prices)
{
    foreach ($product_prices as $branch_id => $price) {
        // Check if a price exists for this branch and product combination
        $existingPrice = $this->where('product_id', $product_id)
            ->where('branch_id', $branch_id)
            ->first();

        if ($existingPrice) {
            // If a price exists, update it
            $this->update($existingPrice['id'], ['price' => $price]);
        } else {
            // If no price exists, insert a new record
            $this->save([
                'product_id' => $product_id,
                'branch_id' => $branch_id,
                'price' => $price,
            ]);
        }
    }
}
public function updateProductQuantity($product_id, $product_quantity)
{
    foreach ($product_quantity as $branch_id => $quantity) {
        // Check if a quantity exists for this branch and product combination


        
        $existingQuantity = $this->where('product_id', $product_id)
            ->where('branch_id', $branch_id)
            ->first();
           

        if ($existingQuantity) {
            // If a quantity exists, update it
            $this->update($existingQuantity['id'], ['quantity' => $quantity]);
        } else {
            // If no quantity exists, insert a new record for this branch and product
            $this->insert([
                'product_id' => $product_id,
                'branch_id' => $branch_id,
                'quantity' => $quantity,
                'price' => 0, // Optionally set a default price here if needed
            ]);
        }
    }
}




}
