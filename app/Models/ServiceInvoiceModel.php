<?php

namespace App\Models;

use CodeIgniter\Model;

class ServiceInvoiceModel extends Model
{
    protected $table = 'service_invoice';
    protected $primaryKey = 'id'; // Assuming the primary key is 'invoice_id'

    protected $allowedFields = [
        'service_number',
        'service_date',
        'cr_name',
        'cr_address',
        'mobile_no',
        'branch_location',
        'payment_type',
        'paid_amount',
        'grand_total',
        'status'
    ];

    public function getInsertID($reset = true)
{
    return parent::getInsertID($reset);
}
   // InvoiceModel.php
// InvoiceModel.php
// public function getInvoiceWithProducts($invoiceId)
// {
//     $invoice = $this->find($invoiceId);

//     if ($invoice) {
//         $invoiceProductModel = new \App\Models\InvoiceProductModel();
//         $products = $invoiceProductModel
//             ->select('products.product_name as product_name', 'invoice_products.quantity', 'invoice_products.price')
//             ->join('products', 'products.id = invoice_products.product_id')
//             ->where('invoice_id', $invoiceId)
//             ->findAll();

//         $invoice['products'] = $products;

//         return $invoice;
//     }

//     return null;
// }
// InvoiceModel.php


  
// InvoiceModel.php
// InvoiceModel.php
// InvoiceModel.php
// InvoiceModel.php
        public function getInvoiceWithService($srinvoiceId)
    {
        $srinvoice = $this->find($srinvoiceId);

        if ($srinvoice) {
            $serviceItemModel = new \App\Models\ServiceItemModel();
            $invoiceService = $serviceItemModel
                ->select(' service_item.price, service_item.price, service.sr_item')
                ->join('service', 'service.id = service_item.service_id')
                ->where('sr_invoice_id', $srinvoiceId)
                ->findAll();

            $srinvoice['service_item'] = $invoiceService;

            return $srinvoice;
        }

        return null;
    }

    public function countTodayServiceInvoices()
    {
        $todayDate = date('Y-m-d');

        // Fetch today's invoices with status 'paid'
        $todayInvoices = $this
            ->where('status', 'paid')
            ->where('service_date', $todayDate)
            ->findAll();

        // Calculate the sum of grand_total for today's invoices
        $todayServiceSales = array_sum(array_column($todayInvoices, 'grand_total'));

        return $todayServiceSales; // Return the calculated value
    }

    public function countMonthlyServiceInvoices()
    {
        $firstDayOfMonth = date('Y-m-01');
        $lastDayOfMonth = date('Y-m-t');

        // Fetch monthly invoices with status 'paid'
        $monthlyInvoices = $this
            ->where('status', 'paid')
            ->where('service_date >=', $firstDayOfMonth)
            ->where('service_date <=', $lastDayOfMonth)
            ->findAll();

        // Calculate the sum of grand_total for monthly invoices
        $monthlyServiceSales = array_sum(array_column($monthlyInvoices, 'grand_total'));

        return $monthlyServiceSales; // Return the calculated value
    }




}
