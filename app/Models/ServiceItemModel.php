<?php

namespace App\Models;

use CodeIgniter\Model;

class ServiceItemModel extends Model
{
    protected $table = 'service_item';
    protected $primaryKey = 'id';
    protected $allowedFields = ['sr_invoice_id', 'service_id', 'price'];

    // Add any other necessary configurations
}
