<?php

namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id'; 

    protected $allowedFields = ['category_name','status'];
    public function countAvailableCategories()
    {
        return $this->where('status', 'available')->countAllResults(); // Count categories with 'available' status
    }
}
