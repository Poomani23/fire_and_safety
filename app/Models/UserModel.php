<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users'; // Specify the table name
    protected $primaryKey = 'id'; // Specify the primary key column name
    protected $allowedFields = [
        'firstname', 'lastname','username', 'email', 'password','confirm_password', 'status'
    ]; // Define fields that are allowed to be mass-assigned


    
}
