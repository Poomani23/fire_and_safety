<?php

namespace App\Models;

use CodeIgniter\Model;

class BranchModel extends Model
{
    protected $table = 'branch';
    protected $primaryKey = 'id'; 

    protected $allowedFields = ['branch_name', 'branch_location', 'status']; 
    public function getAllBranchLocations()
    {
        // Fetch all branch locations
        return $this->where('status', 'active')->findAll(); // Adjust this according to your table structure and method names
    }
    public function countActiveBranches()
    {
        return $this->where('status', 'active')->countAllResults(); // Count branches with 'active' status
    }
     public function getBranchLocationById($branchId)
    {
        $branch = $this->find($branchId);

        if ($branch) {
            return $branch['branch_location'];
        } else {
            return 'Branch Location Not Available';
        }
    }
}
