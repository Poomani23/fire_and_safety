<?php

namespace App\Models;

use CodeIgniter\Model;

class ManagerSettingModel extends Model
{
    protected $table = 'manager_settings';
    protected $primaryKey = 'id';
    protected $allowedFields = ['shop_name', 'profile_img', 'login_img', 'reg_img', 'invoice_prefix'];

    public function getFirstSetting()
    {
        return $this->first();
    }

    public function getLoginImageURL()
    {
        $setting = $this->getFirstSetting();
        return $setting ? $setting['login_img'] : '';
    }

    public function getRegisterImageURL()
    {
        $setting = $this->getFirstSetting();
        return $setting ? $setting['reg_img'] : '';
    }
    
}
