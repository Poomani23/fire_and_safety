<?php
namespace App\Models;

use CodeIgniter\Model;

class ServicePricesModel extends Model
{
    protected $table = 'service_prices';
    protected $primaryKey = 'id';
    protected $allowedFields = ['sr_id', 'sr_item', 'branch_id', 'branch_location', 'price'];

    public function getPricesByServiceId($serviceId)
    {
        return $this->where('sr_id', $serviceId)->findAll();
    }

    // Assuming you have a method in BranchModel to fetch location by branch_id
    public function getLocationByBranchId($branchId)
    {
        $branchModel = new BranchModel();
        $branch = $branchModel->find($branchId);

        if ($branch) {
            return $branch['branch_location'];
        } else {
            return 'Branch Location Not Available';
        }
    }

    public function getAllBranchLocations()
    {
        // Assuming you have a method in BranchModel to fetch all branch locations
        $branchModel = new BranchModel();
        return $branchModel->findAll();
    }

    


}
