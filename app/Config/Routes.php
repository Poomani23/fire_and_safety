<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */


$routes->get('/', 'Home::index');


//dashboard routes
$routes->get('user_dashboard', 'DashboardController::index'); 


//admin dashboard routes
$routes->get('admin_dashboard', 'DashboardController::admin_dashboard', ['filter' => 'auth']);


// Auth routes
$routes->get('login', 'Auth::index'); 
$routes->get('register', 'Auth::register'); 
$routes->post('register', 'Auth::registerUser');
$routes->post('login', 'Auth::loginUser');
$routes->get('logout', 'Auth::logout'); 

// branch routes
$routes->group('', ['filter' => 'auth'], function ($routes) {
    $routes->get('add_branch', 'branch::add_branch'); 
    $routes->post('branch/save', 'branch::save');
    $routes->get('manage_branch', 'branch::manage');
   
   $routes->get('branch/edit/(:num)', 'Branch::edit/$1'); 
   $routes->post('branch/update/(:num)', 'Branch::update/$1');
   
   $routes->get('branch/delete/(:num)', 'Branch::delete/$1');

}); // Apply 'auth' filter to all routes in this group



// role routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('manage_role', 'RoleController::index');

$routes->get('add_role', 'RoleController::add_role');

$routes->post('submit-form', 'RoleController::store');

$routes->get('edit_role/(:num)', 'RoleController::edit/$1');


$routes->post('role/update/(:num)', 'RoleController::update/$1');

$routes->get('delete/(:num)', 'RoleController::delete/$1');
}); // Apply 'auth' filter to all routes in this group


// productroutes
$routes->group('', ['filter' => 'auth'], function ($routes) {

    $routes->get('manage_product', 'ProductController::index');
    $routes->get('add_product', 'ProductController::add_product');
    $routes->post('save_product', 'ProductController::save_product');
    $routes->get('getProductPrices/(:any)', 'ProductController::getProductPrices/$1');
    $routes->get('edit_product/(:num)', 'ProductController::editProduct/$1');
     $routes->post('update/(:num)', 'ProductController::updateProduct/$1');
     $routes->get('delete_product/(:num)', 'ProductController::deleteProduct/$1');
    }); // Apply 'auth' filter to all routes in this group



// Category routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('add_category', 'CategoryController::add_category'); 

$routes->post('category/save', 'CategoryController::save');

$routes->get('manage_category', 'CategoryController::manage');

$routes->get('category/edit/(:num)', 'CategoryController::edit/$1'); 

$routes->post('category/update/(:num)', 'CategoryController::update/$1');

$routes->get('category/delete/(:num)', 'CategoryController::delete/$1');
}); // Apply 'auth' filter to all routes in this group


// service routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('manage_service', 'ServiceController::index');

$routes->get('add_service', 'ServiceController::add_service');

$routes->post('store', 'ServiceController::store');
// routes.php

$routes->get('edit_service/(:num)', 'ServiceController::edit_service/$1');




$routes->post('update_service/(:num)', 'ServiceController::update_service/$1');
// app/config/Routes.php

$routes->get('delete_service/(:num)', 'ServiceController::delete_service/$1');

}); // Apply 'auth' filter to all routes in this group


// invoice routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('manage_invoice', 'InvoiceController::index');

$routes->get('add_invoice', 'InvoiceController::add_invoice');

$routes->post('save_invoice', 'InvoiceController::save_invoice');
$routes->get('edit_pro_invoice/(:num)', 'InvoiceController::edit_invoice/$1');

$routes->post('update_invoice', 'InvoiceController::update_invoice');

$routes->get('delete_pro_invoice/(:num)', 'InvoiceController::delete_pro_invoice/$1');
$routes->post('fetch_invoice_details', 'InvoiceController::fetch_invoice_details');


$routes->get('fetch-customer-and-invoice-details/(:num)', 'PdfController::fetchCustomerAndInvoiceDetails/$1');
 $routes->get('generatePdf/(:num)', 'PdfController::generatePdf/$1');

$routes->get('pdf_invoice', 'InvoiceController::print_invoice_pdf');
$routes->post('save_and_print', 'InvoiceController::save_and_print');
$routes->get('pdf/(:any).pdf', 'InvoiceController::servePdf/$1');

$routes->post('/getProductNamesByBranch', 'InvoiceController::getProductNamesByBranch');
$routes->post('/getProductPrice', 'InvoiceController::getProductPrice');



$routes->post('store', 'InvoiceController::store');

$routes->get('edit_invoice/(:num)', 'InvoiceController::edit/$1');

$routes->post('invoice/update/(:num)', 'InvoiceController::update/$1');

$routes->get('invoice/delete/(:num)', 'InvoiceController::delete/$1');
$routes->post('/invoice-report', 'InvoiceController::displayInvoiceReport');

$routes->get('product_invoice_report', 'InvoiceController::product_invoice_report');

}); // Apply 'auth' filter to all routes in this group


//service invoice routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('manage_service_invoice', 'ServiceInvoiceController::index');
$routes->get('add_service_invoice', 'ServiceInvoiceController::add_service_invoice');
$routes->post('getServiceItemsByBranch', 'ServiceInvoiceController::getServiceItemsByBranch');
$routes->post('/getServicePrice', 'ServiceInvoiceController::getServicePrice');
$routes->post('save_service_invoice', 'ServiceInvoiceController::save_service_invoice');
$routes->get('edit_sr_invoice/(:num)', 'ServiceInvoiceController::edit_sr_invoice/$1');

$routes->post('update_sr_invoice', 'ServiceInvoiceController::update_sr_invoice');

$routes->get('delete_sr_invoice/(:num)', 'ServiceInvoiceController::delete_sr_invoice/$1');
$routes->post('fetch_sr_invoice_details', 'ServiceInvoiceController::fetch_sr_invoice_details');
$routes->get('fetch-customer-and-sr-invoice-details/(:num)', 'ServicePdfController::fetchCustomerAndSrInvoiceDetails/$1');

$routes->get('srgeneratePdf/(:num)', 'ServicePdfController::srgeneratePdf/$1');

$routes->post('/srinvoice-report', 'ServiceInvoiceController::displaySrInvoiceReport');

$routes->get('service_invoice_report', 'ServiceInvoiceController::service_invoice_report');
}); // Apply 'auth' filter to all routes in this group




  
//common setting routes
$routes->group('', ['filter' => 'auth'], function ($routes) {

$routes->get('common_setting', 'CommonSettingController::index');
$routes->post('common_setting/update', 'CommonSettingController::update');


}); // Apply 'auth' filter to all routes in this group

























//Billing manager routes

$routes->group('manager', ['filter' => 'manager'], function ($routes) {
    $routes->get('manager_dashboard', 'ManagerController::manager_dashboard');

    //manager setting routes
    $routes->get('manager_setting', 'ManagerSettingController::index');
    $routes->post('update', 'ManagerSettingController::update');
//invoice routes



$routes->get('manage_invoice', 'Manager\InvoiceController::index');

$routes->post('invoice-report', 'Manager\InvoiceController::displayInvoiceReport');

$routes->get('product_invoice_report', 'Manager\InvoiceController::product_invoice_report');


$routes->post('fetch_invoice_details', 'Manager\InvoiceController::fetch_invoice_details');


$routes->get('fetch-customer-and-invoice-details/(:num)', 'PdfController::fetchCustomerAndInvoiceDetails/$1');
 $routes->get('generatePdf/(:num)', 'PdfController::generatePdf/$1');


 $routes->get('manage_service_invoice', 'Manager\ServiceInvoiceController::index');
 $routes->get('service_invoice_report', 'Manager\ServiceInvoiceController::service_invoice_report');
 $routes->get('service_invoice_report', 'Manager\ServiceInvoiceController::service_invoice_report');

});






$routes->group('billingofficer', ['filter' => 'billingofficer'], function ($routes) {
    $routes->get('billingofficer_dashboard', 'BillingOfficerController::billingofficer_dashboard');
    
    $routes->get('common_setting', 'OfficerSettingController::index');
    $routes->post('update', 'OfficerSettingController::update');

    $routes->get('manage_invoice', 'BillingOfficer\InvoiceController::index');
    $routes->get('add_invoice', 'BillingOfficer\InvoiceController::add_invoice');

$routes->post('save_invoice', 'BillingOfficer\InvoiceController::save_invoice');
$routes->get('edit_pro_invoice/(:num)', 'BillingOfficer\InvoiceController::edit_invoice/$1');

$routes->post('update_invoice', 'BillingOfficer\InvoiceController::update_invoice');

$routes->get('delete_pro_invoice/(:num)', 'BillingOfficer\InvoiceController::delete_pro_invoice/$1');
    $routes->post('invoice-report', 'BillingOfficer\InvoiceController::displayInvoiceReport');
    $routes->get('product_invoice_report', 'BillingOfficer\InvoiceController::product_invoice_report');
    $routes->post('fetch_invoice_details', 'BillingOfficer\InvoiceController::fetch_invoice_details');
    $routes->get('generatePdf/(:num)', 'PdfController::generatePdf/$1');




    //service invoice routes
    $routes->get('manage_service_invoice', 'BillingOfficer\ServiceInvoiceController::index');
    $routes->get('service_invoice_report', 'BillingOfficer\ServiceInvoiceController::service_invoice_report');
    $routes->get('add_service_invoice', 'BillingOfficer\ServiceInvoiceController::add_service_invoice');
    $routes->post('getServiceItemsByBranch', 'BillingOfficer\ServiceInvoiceController::getServiceItemsByBranch');
    $routes->post('/getServicePrice', 'BillingOfficer\ServiceInvoiceController::getServicePrice');
    $routes->post('save_service_invoice', 'BillingOfficer\ServiceInvoiceController::save_service_invoice');
    $routes->get('edit_sr_invoice/(:num)', 'BillingOfficer\ServiceInvoiceController::edit_sr_invoice/$1');
    
    $routes->post('update_sr_invoice', 'BillingOfficer\ServiceInvoiceController::update_sr_invoice');
    
    $routes->get('delete_sr_invoice/(:num)', 'BillingOfficer\ServiceInvoiceController::delete_sr_invoice/$1');


});